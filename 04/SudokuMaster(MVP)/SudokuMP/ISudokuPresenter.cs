﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SudokuMP
{
    public interface ISudokuPresenter
    {
		void ResetBoard();
		void LoadBoard(string task);
		bool BoardSolver(int recurses=0);
		bool SetMember(int index, char value, bool hard = false);
	}
}
