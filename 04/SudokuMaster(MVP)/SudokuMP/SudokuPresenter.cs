﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SudokuMP
{
	public class SudokuPresenter : ISudokuPresenter
	{
		private ISudokuDataModel sudoku;

		//Debug + analysis
		int recursesUsed = 0; //No. of recurse calls from the previous write
		int recurseLevel = 0; //Maximal BoardSolver method recurse level.
		string board_origin; //Saved task string

		public SudokuPresenter()
		{
			sudoku = new SudokuDataModel();
			sudoku.ResetBoard();
		}

		public ISudokuDataModel SudokuBoardData
		{
			get
			{
				return sudoku;
			}
			set
			{
				sudoku = value;
			}
		}

		public void ResetBoard()
		{
			sudoku.ResetBoard();
			sudoku.RaisePropertyChangedEvent();
		}
		/// <summary>
		/// Loads a sudoku task into a matrix. The task is a string containing a sudoku task, where every chracter
		/// represents a single field of sudoku. The non digits and zero reprezents an empty field.
		/// The method raise an exception, if some of digits violate sudoku rules.
		/// </summary>
		/// <param name="task"></param>
		public void LoadBoard(string task)
		{
			board_origin = task; //Save task
			ResetBoard(); //Empty the board
			if (task.Length > 81) task = task.Substring(0, 81);
			bool ok = true;
			int position = 0;
			foreach (char num in task)
			{
				if ("123456789".Contains(num.ToString()))
				{
					ok = sudoku.SetMember(position, num, true);
					if (!ok) throw new Exception("Error: The number " + num + " on position " + position + " invalidate rules.");
				}
				position++;
			}
		}

		/// <summary>
		/// The method locks the valid board values for editing and start the BoardSolver method
		/// </summary>
		/// <returns></returns>
		public void SolveBoard()
		{
			char value;
			for(int i = 0; i < 81; i++)
			{
				if ("123456789".Contains(sudoku.ReadValue(i).ToString()))
				{
					value = sudoku.ReadValue(i);
					sudoku.SetMember(i, value, true);
				}				
			}
			BoardSolver();
		}

		/// <summary>
		/// The method attempts to solve the board.
		/// It is recurse called untill the solution is fond or definitely impossible.
		/// </summary>
		/// <returns>True if succeed.</returns>
		public bool BoardSolver(int recurse = 0)
		{
			//Debug info
			recursesUsed++;
			if (recurseLevel < recurse) recurseLevel = recurse;
			//---

			if (recurse > 80) throw new Exception("Critical: Maximum recurse level reached!");

			if (HeuristicMethodSolver()) return true;

			if (RecursiveTryAndSeeSolver(recurse))
			{
				sudoku.RaisePropertyChangedEvent();
				return true;
			}
			return false;
		}

		/// <summary>
		/// Method iterate board, finding a field with the shortest choices list.
		/// If the combination of empty field (0) and empty choices list occurs, it raise "No solution this way." exception.
		/// Method out return a string copy of actual board values (here for performance reason)
		/// </summary>
		/// <returns>int[3] array with coordinates and the lists' length</returns>
		private int[] FindShortestChoiceList()
		{
			int shortestLength = 10;
			for (int i = 0; i < 81; i++)
			{
				if (sudoku.ReadValue(i) == '0' && sudoku.ReadChoices(i).Length == 0) throw new Exception("No solution this way.");
				if (sudoku.ReadChoices(i).Length > 0 && sudoku.ReadChoices(i).Length < shortestLength)
				{
					return new int[] {i, sudoku.ReadChoices(i).Length };
				}
			}
			return new int[] { 0, 0 };
		}

		/// <summary>
		/// Method tries list of choices and recursively calls the BoardSolver() to find a solution.
		/// Can raise the "Error: Recursive method tried invalid choice! ..." exception.
		/// </summary>
		/// <param name="recurse"></param>
		/// <returns>False means no solution was fond. True means success.</returns>
		private bool RecursiveTryAndSeeSolver(int recurse)
		{
			int[] shortest;
			string saveboard = sudoku.ToString();// SaveBoard();
			try
			{
				shortest = FindShortestChoiceList();
			}
			catch
			{
				return false;
			}

			string choices = sudoku.ReadChoices(shortest[0]);
			foreach (char choice in choices)
			{
				if (sudoku.SetMember(shortest[0], choice, true))
				{
					//The choice is inserted
					if (BoardSolver(recurse + 1)) return true;
				}
				else
				{
					throw new Exception("Error: Recursive method tried invalid choice! :" + shortest[0] + ": " + choice);
				}
				ResetBoard();
				LoadBoard(saveboard);
			}

			return false; //Fail if no other option
		}

		/// <summary>
		/// Method'll fill the fields with only choice left until it can't find any or the board is solved.
		/// </summary>
		/// <returns>True when the board is solved. False otherways.</returns>
		private Boolean HeuristicMethodSolver()
		{
			int position = 0;
			int inserts = 0;
			int zeroValueCount = 0;
			bool inserted;
			while (true)
			{
				if (sudoku.ReadChoices(position).Length == 1)
				{
					inserted = sudoku.SetMember(position, Convert.ToChar(sudoku.ReadChoices(position)));
					if (inserted) inserts++;
				}
				if (sudoku.ReadValue(position) == '0') zeroValueCount++;
				position++;
				if (position>80)
				{
					position = 0;
					if (zeroValueCount == 0) return true;
					zeroValueCount = 0;
					if (inserts == 0) break;
					inserts = 0;
				}
			}
			return false;
		}

		public string ReadChoices(int index)
		{
			return sudoku.ReadChoices(index);
		}

		public bool ReadImmutable(int index)
		{
			return sudoku.ReadImmutable(index);
		}

		public char ReadValue(int x)
		{
			return sudoku.ReadValue(x);
		}

		public bool SetMember(int index, char value, bool hard = false)
		{
			return sudoku.SetMember(index, value, hard);
		}
	}
}
