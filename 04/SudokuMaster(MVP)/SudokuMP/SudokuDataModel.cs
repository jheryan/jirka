﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace SudokuMP
{
	public class SudokuDataModel : ISudokuDataModel, INotifyPropertyChanged, INotifyDataErrorInfo
	{
		char[] board = new char[81]; //Sudoku numbers field
		bool[] immutable = new bool[81]; //True - immutable digit added from task
		string[] possibleChoices = new string[81]; //String contains digit choices for the possition

		public SudokuDataModel()
		{
			ResetBoard();
		}

		public void ResetBoard()
		{
			for (int x = 0; x < 81; x++)
			{
				board[x] = '0';
				immutable[x] = false;
				possibleChoices[x] = "123456789";
			}
		}


		/// <summary>
		/// Inserts the digit into the field with respect to all sudoku rules. Changes possible choices in
		/// affected fields. If hard is true, sets the field immutable.
		/// </summary>
		/// <param name="index">column</param>
		/// <param name="value">value</param>
		/// <param name="hard">immutability</param>
		/// <returns></returns>
		public bool SetMember(int index, char value, bool hard = false)
		{
			if (ReadImmutable(index))
			{
				return false;
			}
			string oldnum = ReadValue(index).ToString();
			if (ReadChoices(index).Contains(value.ToString()) | oldnum == value.ToString())
			{
				WriteValue(index, value);
				WriteImmutable(index, hard);
				EditRowAndColumnChoices(index, value, oldnum);
				EditSquareCourtChoices(index, value, oldnum);
				if (hard) WriteChoices(index, "");
				return true;
			}
			return false;
		}

		private void EditRowAndColumnChoices(int index, char newNum, string oldNum)
		{
			int column = index % 9;
			int row = index - column;
			for (int z = 0; z < 9; z++)
			{

				AddChoice(row, oldNum);
				RemoveChoice(row, newNum.ToString());
				AddChoice(column, oldNum);
				RemoveChoice(column, newNum.ToString());
				row++;
				column += 9;
			}
		}

		private void EditSquareCourtChoices(int index, char newNum, string oldNum)
		{
			int ax = (index % 9) / 3 * 3;
			int ay = index / 9 / 3 * 3;
			index = ay * 9 + ax;

			for (int i = 0; i < 3; i++)
			{
				AddChoice(index + i, oldNum);
				AddChoice(index + 9 + i, oldNum);
				AddChoice(index + 18 + i, oldNum);
				RemoveChoice(index + i, newNum.ToString());
				RemoveChoice(index + 9 + i, newNum.ToString());
				RemoveChoice(index + 18 + i, newNum.ToString());
			}
		}

		//Data read handlers

		/// <summary>
		/// Reads choices list for the field.
		/// </summary>
		/// <param name="index">Field index</param>
		/// <returns>A string containing all valid digits for the field.</returns>
		public string ReadChoices(int index)
		{
			return possibleChoices[index];
		}
		public string ReadChoices(int x, int y)
		{
			return ReadChoices(x + y * 9);
		}

		private void WriteChoices(int index, string choices)
		{
			possibleChoices[index] = choices;
		}

		/// <summary>
		/// Add choice to the list of choices for the field X. If choice is valid and present,
		/// it will not change the list.
		/// </summary>
		/// <param name="index">Field index</param>
		/// <param name="choice">False if the choice is invalid. True otherway.</param>
		/// <returns></returns>
		private bool AddChoice(int index, string choice)
		{
			string opt = Convert.ToString(choice);
			if ("123456789".Contains(opt))
			{
				if (ReadChoices(index).Contains(opt)) return true;
				WriteChoices(index, ReadChoices(index) + opt);
			}
			else
			{
				return false;
			}
			return true;
		}

		/// <summary>
		/// Remove a choice from the list for the field.
		/// </summary>
		/// <param name="index">Field list</param>
		/// <param name="choice">Digit to remove</param>
		private void RemoveChoice(int index, string choice)
		{
			possibleChoices[index] = ReadChoices(index).Replace(choice, "");
		}

		/// <summary>
		/// Reads immutable status for the field.
		/// </summary>
		/// <param name="index">Field index</param>
		/// <returns>True/False for Immutable/Mutable</returns>
		public bool ReadImmutable(int index)
		{
			return immutable[index];
		}
		public bool ReadImmutable(int x, int y)
		{
			return ReadImmutable(x + y * 9);
		}

		private void WriteImmutable(int index, bool value)
		{
			immutable[index] = value;
		}

		/// <summary>
		/// Read an actual field digit value.
		/// </summary>
		/// <param name="x">Field index</param>
		/// <returns>Returns digit value of the field as unsigned short integer.</returns>
		public char ReadValue(int x)
		{
			return board[x];
		}
		public char ReadValue(int x, int y)
		{
			return ReadValue(x + y * 9);
		}

		private void WriteValue(int index, char value)
		{
			board[index] = value;
		}

		public new string ToString()
		{
			return String.Join("", board);
		}

		///Iterator
		public char this[int index]
		{
			get
			{
				return ReadValue(index);
			}
			set
			{
				if(!SetMember(index, value))
				{
						List<string> errors = new List<string>();
						errors.Add("The character breaks the sudoku rules.");
						SetErrors("num"+index, errors);
					}
					else
					{
						ClearErrors("num"+index);
					}
				OnPropertyChanged(null);
			}
		}

		

		public void RaisePropertyChangedEvent(string sender=null)
		{
			OnPropertyChanged(sender);
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler handler = PropertyChanged;
			if (handler != null)
			{
				handler(this, new PropertyChangedEventArgs(name));
			}
		}


		private Dictionary<string, List<string>> errors = new Dictionary<string, List<string>>();

		public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
		private void SetErrors(string propertyName, List<string> propertyErrors)
		{
			// Clear any errors that already exist for this property.
			errors.Remove(propertyName);
			// Add the list collection for the specified property.
			errors.Add(propertyName, propertyErrors);
			// Raise the error-notification event.
			if (ErrorsChanged != null)
				ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
		}
		private void ClearErrors(string propertyName)
		{
			// Remove the error list for this property.
			errors.Remove(propertyName);
			// Raise the error-notification event.
			if (ErrorsChanged != null)
				ErrorsChanged(this, new DataErrorsChangedEventArgs(propertyName));
		}
		public IEnumerable GetErrors(string propertyName)
		{
			if (string.IsNullOrEmpty(propertyName))
			{
				// Provide all the error collections.
				return (errors.Values);
			}
			else
			{
				// Provice the error collection for the requested property
				// (if it has errors).
				if (errors.ContainsKey(propertyName))
				{
					return (errors[propertyName]);
				}
				else
				{
					return null;
				}
			}
		}
		public bool HasErrors
		{
			get
			{
				// Indicate whether the entire Product object is error-free.
				return (errors.Count > 0);
			}
		}


	}

}
