﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using SudokuMP;

namespace SudokuMaster_WFP_
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        TextBox firstTextBox;

        public MainWindow()
        {
			InitializeComponent();
			CreateBoard();
			this.DataContext = new SudokuMP.SudokuPresenter();
			
        }

		private void CreateBoard()
		{
			int cornerX, cornerY, position = 0;
			Border b;
			double th1 = .4, th2 = 1.5;

			for(int y=0; y<9; y++)
			{
				for(int x = 0; x < 9; x++)
				{
					cornerX = x % 3;
					cornerY = y % 3;
					b = GetBorderElement(cornerX, cornerY, th1, th2);
					b.Child = GetTextField(position);
					position++;
					BoardGrid.Children.Add(b);
				}
			}
			
			//BoardGrid.Children.Add();
		}

		private Border GetBorderElement(int cornerX, int cornerY, double thin, double bold)
		{
			Thickness thick;
			Border b;
			b = new Border();
			b.BorderBrush = Brushes.Black;

			if ((cornerX + cornerY) == 0) thick = new Thickness(bold, bold, thin, thin);
			else if (cornerX == 1 && cornerY == 0) thick = new Thickness(thin, bold, thin, thin);
			else if (cornerX == 2 && cornerY == 0) thick = new Thickness(thin, bold, bold, thin);
			else if (cornerX == 0 && cornerY == 1) thick = new Thickness(bold, thin, thin, thin);
			else if (cornerX == 2 && cornerY == 1) thick = new Thickness(thin, thin, bold, thin);
			else if (cornerX == 0 && cornerY == 2) thick = new Thickness(bold, thin, thin, bold);
			else if (cornerX == 1 && cornerY == 2) thick = new Thickness(thin, thin, thin, bold);
			else if (cornerX == 2 && cornerY == 2) thick = new Thickness(thin, thin, bold, bold);
			else thick = new Thickness(thin);
			b.BorderThickness = thick;
			b.Margin = new Thickness(0);
			b.Padding = new Thickness(1);
			return b;
		}

		private TextBox GetTextField(int positionInBoard)
		{
			TextBox b = new TextBox();
			b.FontSize = 20;
			b.HorizontalContentAlignment = HorizontalAlignment.Center;
			b.VerticalContentAlignment = VerticalAlignment.Center;
			Brush myBrush;
			//myBrush.Opacity = 0;
			b.BorderThickness = new Thickness(0);
			Binding bin = new Binding();
			string s = "SudokuBoardData[" + positionInBoard + "]";
			//Console.WriteLine(s);
			bin.Path = new PropertyPath(s);
			bin.ValidatesOnNotifyDataErrors = true;
			bin.NotifyOnValidationError = true;
			bin.Converter = new SudokuBoardToDataModelConverter();
			
			b.SetBinding(TextBox.TextProperty, bin);
			b.MaxLength = 1;
            if (positionInBoard == 0) firstTextBox = b;
			return b;
		}

		private void SolveButton_Click(object sender, RoutedEventArgs e)
		{
			(this.DataContext as SudokuPresenter).SolveBoard();
			//OnPropertyChanged();
		}

		private void ResetButton_Click(object sender, RoutedEventArgs e)
		{
			(this.DataContext as SudokuPresenter).ResetBoard();
		}

        private void MainWindow_PreviewKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.D0:
                case Key.D1:
                case Key.D2:
                case Key.D3:
                case Key.D4:
                case Key.D5:
                case Key.D6:
                case Key.D7:
                case Key.D8:
                case Key.D9:
                case Key.NumPad0:
                case Key.NumPad1:
                case Key.NumPad2:
                case Key.NumPad3:
                case Key.NumPad4:
                case Key.NumPad5:
                case Key.NumPad6:
                case Key.NumPad7:
                case Key.NumPad8:
                case Key.NumPad9:
                    TextBox s = e.Source as TextBox;
                    if (s != null)
                    {
                        s.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                    }

                    e.Handled = true;
                    break;
                default:
                    e.Handled = false;
                    break;
            }
        }

        private void OnLoaded(object sender, RoutedEventArgs e)
        {
            // Sets keyboard focus on the first Button in the sample.
            Keyboard.Focus(firstTextBox);
        }
    }
}
