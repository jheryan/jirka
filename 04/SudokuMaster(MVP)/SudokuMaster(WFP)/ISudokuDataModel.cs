﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuMP
{
    public interface ISudokuDataModel
    {
		void ResetBoard();
        string ReadChoices(int index);
        bool ReadImmutable(int index);
        char ReadValue(int x);
        bool SetMember(int index, char value, bool hard = false);
		void RaisePropertyChangedEvent(string sender=null);
		string ToString();
    }
}
