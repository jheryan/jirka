﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SudokuMP;
using System.IO;

namespace SudokuMaster_MVP_UT
{
    [TestClass]
    public class SudokuMasterWfpTests
    {
        [TestMethod]
        public void DataModelTest_inserting()
        {
            //Tests: inserting and rewriting digits
            //Value tests
            ISudokuDataModel sudoku = new SudokuDataModel();
            sudoku.SetMember(10, '5');
            Assert.AreEqual('5', sudoku.ReadValue(10));
            Assert.AreEqual("12346789", sudoku.ReadChoices(20));
            sudoku.SetMember(10, '6', true);
            Assert.AreEqual('6', sudoku.ReadValue(10));
            Assert.IsFalse(sudoku.SetMember(10, '7'));
            Assert.AreEqual('6', sudoku.ReadValue(10));

			//Choices in square tests
			bool result;
			string fail;
			Assert.AreEqual("12347895", sudoku.ReadChoices(0));//First in square
			//result = !sudoku.ReadChoices(0).Contains("6");
			//string fail = "First in square.";
			//Assert.AreEqual("", (result) ? "" : fail);
			result = !sudoku.ReadChoices(20).Contains("6");
            fail = "Last in square.";
            Assert.AreEqual("", (result) ? "" : fail);
            result = sudoku.ReadChoices(21).Contains("6");
            fail = "Out of square and row.";
            Assert.AreEqual("", (result) ? "" : fail);

            //Choices in row and column
            Assert.IsFalse(sudoku.SetMember(20, '6'));
            sudoku.SetMember(30, '5');
            result = !sudoku.ReadChoices(35).Contains("5");
            fail = "Last in row.";
            Assert.AreEqual("", (result) ? "" : fail);
            result = sudoku.ReadChoices(36).Contains("5");
            fail = "First in next row.";
            Assert.AreEqual("", (result) ? "" : fail);
            result = !sudoku.ReadChoices(27).Contains("5");
            fail = "First in row.";
            Assert.AreEqual("", (result) ? "" : fail);
            result = sudoku.ReadChoices(26).Contains("5");
            fail = "Last in previous row.";
            Assert.AreEqual("", (result) ? "" : fail);

            sudoku.SetMember(43, '1');
            result = !sudoku.ReadChoices(7).Contains("1");
            fail = "First in column.";
            Assert.AreEqual("", (result) ? "" : fail);
            result = sudoku.ReadChoices(78).Contains("1");
            fail = "Last in previous column.";
            Assert.AreEqual("", (result) ? "" : fail);

            //Assert.Fail();
        }

		//Presenter tests
		[TestMethod()]
		public void SudokuBoardTest()
		{
			SudokuPresenter sudoku = new SudokuPresenter();
			Assert.AreEqual('0', sudoku.ReadValue(2));
			Assert.AreEqual("123456789", sudoku.ReadChoices(2));
		}

		[TestMethod()]
		public void setMemeberTest()
		{
			//Tests: insert, not insert invalid and reduce options for the row and the column
			SudokuPresenter sudoku = new SudokuPresenter();
			sudoku.SetMember(3, '5', true);
			sudoku.SetMember(13, '4');
			Assert.AreEqual("1236789", sudoku.ReadChoices(23));
			Assert.AreEqual("12346789", sudoku.ReadChoices (8));
			Assert.AreEqual("12356789", sudoku.ReadChoices(17));
		}

		[TestMethod()]
		public void loadBoardTest()
		{
			//Test of board loading
			SudokuPresenter sudoku = new SudokuPresenter();
			Assert.AreEqual("123456789", sudoku.ReadChoices(0));
			sudoku.LoadBoard("350004600000007140910060720470100002093020450800003016049080071081500000002700084");
			Assert.AreEqual('5', sudoku.ReadValue(43));
			Assert.AreEqual('8', sudoku.ReadValue(45));
			Assert.AreEqual('4', sudoku.ReadValue(80));
		}

		/*
		[TestMethod()]
		public void loadBoardTest_debug()
		{
			//Test of board loading
			ISudokuPresenter sudoku = new SudokuPresenter();
			Assert.AreEqual("123456789", sudoku.ReadChoices(0));
			sudoku.LoadBoard("350004600000007140910060720470100002093020450800003016049080071081500000002700084");
			WriteBoard("DBG", (SudokuPresenter)sudoku, "350004600 000007140 910060720 470100002 093020450 800003016049080071081500000002700084");
			Assert.AreEqual('5', sudoku.ReadValue(1));
			Assert.AreEqual('8', sudoku.ReadValue(45));
			Assert.AreEqual('4', sudoku.ReadValue(80));
		} //*/

		[TestMethod()]
		public void loadBoardTest2()
		{
			//Test loading not fully correct board (string length, use of letters for empty)
			SudokuPresenter sudoku = new SudokuPresenter();
			Assert.AreEqual("123456789", sudoku.ReadChoices(0));
			sudoku.LoadBoard("35000460000000714091006072047010000209302045080h00301604908007108150000000270008451");
			Assert.AreEqual('4', sudoku.ReadValue(80));
		}

		[TestMethod()]
		public void BoardSolverTest()
		{
			//Tests with results writen to file(s). Check the file Sudoku_board...txt (see SudokuBoard.WriteBoard)
			SudokuPresenter sudoku = new SudokuPresenter();

			sudoku.LoadBoard("...4...57....17..6..5...8......9...17....3..4..8..5...65.83..7..1...6....9..7....");//319
																												  //sudoku.BoardSolver();
			Assert.IsTrue(sudoku.BoardSolver());
			WriteBoard("319",sudoku); //Not implemented board to file method.

			sudoku.ResetBoard();
			sudoku.LoadBoard("3,,,6,,,2,,,9,,,,88,52,,47,,7,,,,,,,1,8,,,6,7,,9,57,1,,,,,1,,,,,,76,,,,,,6,,,,,,3");//320
			Assert.IsTrue(sudoku.BoardSolver());
			WriteBoard("320",sudoku); //Not implemented board to file method.

			sudoku.ResetBoard();
			sudoku.LoadBoard(",,,16,,3,,,,,8,5,9,58,,,,7,9,,,,,,,,,,183,,,,,,4,,,,52,,,,4,6,7,6,,,12,,8,,,,,,,1");//323
			Assert.IsTrue(sudoku.BoardSolver());
			WriteBoard("323",sudoku); //Not implemented board to file method.

		}

		[TestMethod()]
		public void BoardSolverTest2()
		{
			//Tests with results writen to file(s). Check the file Sudoku_board...txt (see SudokuBoard.WriteBoard)
			SudokuPresenter sudoku = new SudokuPresenter();

			sudoku.LoadBoard("..........456...87...5");//
																												  //sudoku.BoardSolver();
			Assert.IsTrue(sudoku.BoardSolver());
			WriteBoard("c001", sudoku); //Not implemented board to file method.

			sudoku.ResetBoard();
		}

		[TestMethod()]
		public void EmptyBoardSolverTest()
		{
			//Tests with results writen to file(s). Check the file Sudoku_board...txt (see SudokuBoard.WriteBoard)
			SudokuPresenter sudoku = new SudokuPresenter();

			//sudoku.LoadBoard("...4...57....17..6..5...8......9...17....3..4..8..5...65.83..7..1...6....9..7....");//319
																												  //sudoku.BoardSolver();
			Assert.IsTrue(sudoku.BoardSolver());
			WriteBoard("empty", sudoku); //Not implemented board to file method.
		}

		//Write to file (debug)
		/// <summary>
		/// Method writes the solution of board into a Sudoku_Board_values<suffix>.txt file.
		/// Intended for debuging.
		/// </summary>
		/// <param name="suffix">filename suffix</param>
		public void WriteBoard(string suffix = "", SudokuPresenter boardObj=null, string board_origin="")
		{
			TextWriter s = File.CreateText("Sudoku_Board_values" + suffix + ".txt");

			///*
			s.WriteLine("Sudoku board:"+suffix);
			s.WriteLine("Task: " + board_origin);
			s.WriteLine("Solution:");
			string line = "", line2 = "", line3 = "";
			line += " " + boardObj.ReadValue(0).ToString();
			line2 += " (" + boardObj.ReadChoices(0).Length + ")";
			line3 += "(" + boardObj.ReadChoices(0) + ")";
			for (int a = 1; a < 81; a++)
			{
				if (a % 9 == 0)
				{
					s.WriteLine(line + " |" + line2 + " |" + line3);
					//s.WriteLine(line);
					line = "";
					line2 = "";
					line3 = "";
				}
				line += " " + boardObj.ReadValue(a).ToString();
				line2 += " (" + boardObj.ReadChoices(a).Length + ")";
				line3 += "(" + boardObj.ReadChoices(a) + ")";
			}
			//s.WriteLine("BoardSolver calls count:" + recursesUsed + "   Maximum recurse level:" + recurseLevel);
			//*/  
			//s.WriteLine("Shortest:" + shortest[0] + "," + shortest[1] + " length:" + shortest[2]);
			s.Close();
			//recurseLevel = 0;
			//recursesUsed = 0;

		}
	}
}
