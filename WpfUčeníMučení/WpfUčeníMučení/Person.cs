﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfUčeníMučení
{
	public class Person
	{
		public string Name { get; set;}
		public DateTime Birthday { get; set; }

		public Person() { }
		public Person(string name, DateTime birthday)
		{
			Name = name;
			Birthday = birthday;
		}

		public int Age
		{
			get
			{
				DateTime today = DateTime.Today;
				int age = today.Year - Birthday.Year;
				if (today < Birthday.AddYears(age)) age--;
				return age;
			}
		}

		public int DaysLeft
		{
			get
			{
				DateTime today = DateTime.Today;
				DateTime nextBirthday = Birthday.AddYears(Age + 1);
				TimeSpan difference = nextBirthday - DateTime.Today;

				return Convert.ToInt32(difference.TotalDays);
			}
		}

		public override string ToString()
		{
			return Name;
		}
	}
}
