﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Xml.Serialization;

namespace WpfUčeníMučení
{
	public class PersonManager:INotifyPropertyChanged
	{
		private Person nearestPerson;
		private string path = "PeopleList.xml";

		public ObservableCollection<Person> People { get; set; }

		public PersonManager()
		{
			//People = new ObservableCollection<Person>();
			People = Load();
			FindNearest();
			//this.Add("Tester", Convert.ToDateTime("5.12.1956"));
		}

		public Person NearestPerson
		{
			get
			{
				return nearestPerson;
			}
			set
			{
				nearestPerson = value;
				OnPropertyChanged("NearestPerson");
			}
		}

		public DateTime CurrentDate
		{
			get
			{
				return DateTime.Now;
			}
		}

		public void FindNearest()
		{
			var sorted = People.OrderBy(p => p.DaysLeft);
			if (sorted.Count() > 0)
			{
				NearestPerson = sorted.First();
			}
			else
			{
				NearestPerson = null;
			}
		}

		public void Add(string name, DateTime? birthDate)
		{
			if (name.Length < 3)
			{
				throw new ArgumentException("Příliš krátké jméno.");
			}
			if (birthDate == null) throw new ArgumentException("Datum narození nebylo zadáno.");
			if (birthDate.Value.Date > DateTime.Today) throw new ArgumentException("Datum narození nesmí být v budoucnosti.");
			Person human = new Person(name, birthDate.Value.Date);
			People.Add(human);
			FindNearest();
		}

		public void Remove(Person human)
		{
			People.Remove(human);
			FindNearest();
		}

		public void Save()
		{
			XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<Person>));
			using (StreamWriter wr = new StreamWriter(path))
			{
				xs.Serialize(wr, People);
			}
		}

		public ObservableCollection<Person> Load()
		{
			try
			{
				XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<Person>));
				using (StreamReader sr = new StreamReader(path))
				{
					return (ObservableCollection<Person>)xs.Deserialize(sr);
				}
			}
			catch
			{
				return new ObservableCollection<Person>();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected void OnPropertyChanged(string propertyName)
		{
			if (PropertyChanged != null)
			{
				PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
			}	
		}
	}
}
