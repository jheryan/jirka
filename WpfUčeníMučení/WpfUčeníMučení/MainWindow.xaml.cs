﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfUčeníMučení
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class GummiWokno : Window
	{
		private PersonManager peopleManager = new PersonManager();
		public GummiWokno()
		{
			InitializeComponent();
			DataContext = peopleManager;
		}

		private void addButton_Click(object sender, RoutedEventArgs e)
		{
			PersonWindow windowPerson = new PersonWindow(peopleManager);
			windowPerson.ShowDialog();
		}

		private void removeButton_Click(object sender, RoutedEventArgs e)
		{
			if (peopleListBox.SelectedItem != null)
			{
				peopleManager.Remove((Person)peopleListBox.SelectedItem);
			}
		}
	}
}
