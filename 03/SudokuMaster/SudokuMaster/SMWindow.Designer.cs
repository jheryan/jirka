﻿namespace SudokuMaster
{
    partial class SMWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.field00 = new System.Windows.Forms.TextBox();
            this.field10 = new System.Windows.Forms.TextBox();
            this.field20 = new System.Windows.Forms.TextBox();
            this.field30 = new System.Windows.Forms.TextBox();
            this.field40 = new System.Windows.Forms.TextBox();
            this.field50 = new System.Windows.Forms.TextBox();
            this.field60 = new System.Windows.Forms.TextBox();
            this.field70 = new System.Windows.Forms.TextBox();
            this.field80 = new System.Windows.Forms.TextBox();
            this.field81 = new System.Windows.Forms.TextBox();
            this.field71 = new System.Windows.Forms.TextBox();
            this.field61 = new System.Windows.Forms.TextBox();
            this.field51 = new System.Windows.Forms.TextBox();
            this.field41 = new System.Windows.Forms.TextBox();
            this.field31 = new System.Windows.Forms.TextBox();
            this.field21 = new System.Windows.Forms.TextBox();
            this.field11 = new System.Windows.Forms.TextBox();
            this.field01 = new System.Windows.Forms.TextBox();
            this.field82 = new System.Windows.Forms.TextBox();
            this.field72 = new System.Windows.Forms.TextBox();
            this.field62 = new System.Windows.Forms.TextBox();
            this.field52 = new System.Windows.Forms.TextBox();
            this.field42 = new System.Windows.Forms.TextBox();
            this.field32 = new System.Windows.Forms.TextBox();
            this.field22 = new System.Windows.Forms.TextBox();
            this.field12 = new System.Windows.Forms.TextBox();
            this.field02 = new System.Windows.Forms.TextBox();
            this.field85 = new System.Windows.Forms.TextBox();
            this.field75 = new System.Windows.Forms.TextBox();
            this.field65 = new System.Windows.Forms.TextBox();
            this.field55 = new System.Windows.Forms.TextBox();
            this.field45 = new System.Windows.Forms.TextBox();
            this.field35 = new System.Windows.Forms.TextBox();
            this.field25 = new System.Windows.Forms.TextBox();
            this.field15 = new System.Windows.Forms.TextBox();
            this.field05 = new System.Windows.Forms.TextBox();
            this.field84 = new System.Windows.Forms.TextBox();
            this.field74 = new System.Windows.Forms.TextBox();
            this.field64 = new System.Windows.Forms.TextBox();
            this.field54 = new System.Windows.Forms.TextBox();
            this.field44 = new System.Windows.Forms.TextBox();
            this.field34 = new System.Windows.Forms.TextBox();
            this.field24 = new System.Windows.Forms.TextBox();
            this.field14 = new System.Windows.Forms.TextBox();
            this.field04 = new System.Windows.Forms.TextBox();
            this.field83 = new System.Windows.Forms.TextBox();
            this.field73 = new System.Windows.Forms.TextBox();
            this.field63 = new System.Windows.Forms.TextBox();
            this.field53 = new System.Windows.Forms.TextBox();
            this.field43 = new System.Windows.Forms.TextBox();
            this.field33 = new System.Windows.Forms.TextBox();
            this.field23 = new System.Windows.Forms.TextBox();
            this.field13 = new System.Windows.Forms.TextBox();
            this.field03 = new System.Windows.Forms.TextBox();
            this.field88 = new System.Windows.Forms.TextBox();
            this.field78 = new System.Windows.Forms.TextBox();
            this.field68 = new System.Windows.Forms.TextBox();
            this.field58 = new System.Windows.Forms.TextBox();
            this.field48 = new System.Windows.Forms.TextBox();
            this.field38 = new System.Windows.Forms.TextBox();
            this.field28 = new System.Windows.Forms.TextBox();
            this.field18 = new System.Windows.Forms.TextBox();
            this.field08 = new System.Windows.Forms.TextBox();
            this.field87 = new System.Windows.Forms.TextBox();
            this.field77 = new System.Windows.Forms.TextBox();
            this.field67 = new System.Windows.Forms.TextBox();
            this.field57 = new System.Windows.Forms.TextBox();
            this.field47 = new System.Windows.Forms.TextBox();
            this.field37 = new System.Windows.Forms.TextBox();
            this.field27 = new System.Windows.Forms.TextBox();
            this.field17 = new System.Windows.Forms.TextBox();
            this.field07 = new System.Windows.Forms.TextBox();
            this.field86 = new System.Windows.Forms.TextBox();
            this.field76 = new System.Windows.Forms.TextBox();
            this.field66 = new System.Windows.Forms.TextBox();
            this.field56 = new System.Windows.Forms.TextBox();
            this.field46 = new System.Windows.Forms.TextBox();
            this.field36 = new System.Windows.Forms.TextBox();
            this.field26 = new System.Windows.Forms.TextBox();
            this.field16 = new System.Windows.Forms.TextBox();
            this.field06 = new System.Windows.Forms.TextBox();
            this.buttonSolve = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // field00
            // 
            this.field00.AcceptsTab = true;
            this.field00.Location = new System.Drawing.Point(12, 12);
            this.field00.MaxLength = 1;
            this.field00.Name = "field00";
            this.field00.Size = new System.Drawing.Size(14, 20);
            this.field00.TabIndex = 0;
            this.field00.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field00.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field00.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field00.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field10
            // 
            this.field10.AcceptsTab = true;
            this.field10.Location = new System.Drawing.Point(32, 12);
            this.field10.MaxLength = 1;
            this.field10.Name = "field10";
            this.field10.Size = new System.Drawing.Size(14, 20);
            this.field10.TabIndex = 1;
            this.field10.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field10.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field10.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field10.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field20
            // 
            this.field20.AcceptsTab = true;
            this.field20.Location = new System.Drawing.Point(52, 12);
            this.field20.MaxLength = 1;
            this.field20.Name = "field20";
            this.field20.Size = new System.Drawing.Size(14, 20);
            this.field20.TabIndex = 2;
            this.field20.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field20.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field20.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field20.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field30
            // 
            this.field30.AcceptsTab = true;
            this.field30.Location = new System.Drawing.Point(72, 12);
            this.field30.MaxLength = 1;
            this.field30.Name = "field30";
            this.field30.Size = new System.Drawing.Size(14, 20);
            this.field30.TabIndex = 3;
            this.field30.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field30.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field30.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field30.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field40
            // 
            this.field40.AcceptsTab = true;
            this.field40.Location = new System.Drawing.Point(92, 12);
            this.field40.MaxLength = 1;
            this.field40.Name = "field40";
            this.field40.Size = new System.Drawing.Size(14, 20);
            this.field40.TabIndex = 4;
            this.field40.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field40.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field40.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field40.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field50
            // 
            this.field50.AcceptsTab = true;
            this.field50.Location = new System.Drawing.Point(112, 12);
            this.field50.MaxLength = 1;
            this.field50.Name = "field50";
            this.field50.Size = new System.Drawing.Size(14, 20);
            this.field50.TabIndex = 5;
            this.field50.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field50.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field50.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field50.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field60
            // 
            this.field60.AcceptsTab = true;
            this.field60.Location = new System.Drawing.Point(132, 12);
            this.field60.MaxLength = 1;
            this.field60.Name = "field60";
            this.field60.Size = new System.Drawing.Size(14, 20);
            this.field60.TabIndex = 6;
            this.field60.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field60.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field60.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field60.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field70
            // 
            this.field70.AcceptsTab = true;
            this.field70.Location = new System.Drawing.Point(152, 12);
            this.field70.MaxLength = 1;
            this.field70.Name = "field70";
            this.field70.Size = new System.Drawing.Size(14, 20);
            this.field70.TabIndex = 7;
            this.field70.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field70.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field70.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field70.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field80
            // 
            this.field80.AcceptsTab = true;
            this.field80.Location = new System.Drawing.Point(172, 12);
            this.field80.MaxLength = 1;
            this.field80.Name = "field80";
            this.field80.Size = new System.Drawing.Size(14, 20);
            this.field80.TabIndex = 8;
            this.field80.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field80.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field80.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field80.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field81
            // 
            this.field81.AcceptsTab = true;
            this.field81.Location = new System.Drawing.Point(172, 38);
            this.field81.MaxLength = 1;
            this.field81.Name = "field81";
            this.field81.Size = new System.Drawing.Size(14, 20);
            this.field81.TabIndex = 17;
            this.field81.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field81.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field81.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field81.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field71
            // 
            this.field71.AcceptsTab = true;
            this.field71.Location = new System.Drawing.Point(152, 38);
            this.field71.MaxLength = 1;
            this.field71.Name = "field71";
            this.field71.Size = new System.Drawing.Size(14, 20);
            this.field71.TabIndex = 16;
            this.field71.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field71.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field71.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field71.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field61
            // 
            this.field61.AcceptsTab = true;
            this.field61.Location = new System.Drawing.Point(132, 38);
            this.field61.MaxLength = 1;
            this.field61.Name = "field61";
            this.field61.Size = new System.Drawing.Size(14, 20);
            this.field61.TabIndex = 15;
            this.field61.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field61.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field61.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field61.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field51
            // 
            this.field51.AcceptsTab = true;
            this.field51.Location = new System.Drawing.Point(112, 38);
            this.field51.MaxLength = 1;
            this.field51.Name = "field51";
            this.field51.Size = new System.Drawing.Size(14, 20);
            this.field51.TabIndex = 14;
            this.field51.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field51.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field51.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field51.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field41
            // 
            this.field41.AcceptsTab = true;
            this.field41.Location = new System.Drawing.Point(92, 38);
            this.field41.MaxLength = 1;
            this.field41.Name = "field41";
            this.field41.Size = new System.Drawing.Size(14, 20);
            this.field41.TabIndex = 13;
            this.field41.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field41.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field41.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field41.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field31
            // 
            this.field31.AcceptsTab = true;
            this.field31.Location = new System.Drawing.Point(72, 38);
            this.field31.MaxLength = 1;
            this.field31.Name = "field31";
            this.field31.Size = new System.Drawing.Size(14, 20);
            this.field31.TabIndex = 12;
            this.field31.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field31.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field31.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field31.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field21
            // 
            this.field21.AcceptsTab = true;
            this.field21.Location = new System.Drawing.Point(52, 38);
            this.field21.MaxLength = 1;
            this.field21.Name = "field21";
            this.field21.Size = new System.Drawing.Size(14, 20);
            this.field21.TabIndex = 11;
            this.field21.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field21.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field21.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field21.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field11
            // 
            this.field11.AcceptsTab = true;
            this.field11.Location = new System.Drawing.Point(32, 38);
            this.field11.MaxLength = 1;
            this.field11.Name = "field11";
            this.field11.Size = new System.Drawing.Size(14, 20);
            this.field11.TabIndex = 10;
            this.field11.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field11.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field11.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field11.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field01
            // 
            this.field01.AcceptsTab = true;
            this.field01.Location = new System.Drawing.Point(12, 38);
            this.field01.MaxLength = 1;
            this.field01.Name = "field01";
            this.field01.Size = new System.Drawing.Size(14, 20);
            this.field01.TabIndex = 9;
            this.field01.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field01.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field01.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field01.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field82
            // 
            this.field82.AcceptsTab = true;
            this.field82.Location = new System.Drawing.Point(172, 64);
            this.field82.MaxLength = 1;
            this.field82.Name = "field82";
            this.field82.Size = new System.Drawing.Size(14, 20);
            this.field82.TabIndex = 26;
            this.field82.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field82.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field82.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field82.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field72
            // 
            this.field72.AcceptsTab = true;
            this.field72.Location = new System.Drawing.Point(152, 64);
            this.field72.MaxLength = 1;
            this.field72.Name = "field72";
            this.field72.Size = new System.Drawing.Size(14, 20);
            this.field72.TabIndex = 25;
            this.field72.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field72.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field72.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field72.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field62
            // 
            this.field62.AcceptsTab = true;
            this.field62.Location = new System.Drawing.Point(132, 64);
            this.field62.MaxLength = 1;
            this.field62.Name = "field62";
            this.field62.Size = new System.Drawing.Size(14, 20);
            this.field62.TabIndex = 24;
            this.field62.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field62.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field62.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field62.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field52
            // 
            this.field52.AcceptsTab = true;
            this.field52.Location = new System.Drawing.Point(112, 64);
            this.field52.MaxLength = 1;
            this.field52.Name = "field52";
            this.field52.Size = new System.Drawing.Size(14, 20);
            this.field52.TabIndex = 23;
            this.field52.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field52.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field52.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field52.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field42
            // 
            this.field42.AcceptsTab = true;
            this.field42.Location = new System.Drawing.Point(92, 64);
            this.field42.MaxLength = 1;
            this.field42.Name = "field42";
            this.field42.Size = new System.Drawing.Size(14, 20);
            this.field42.TabIndex = 22;
            this.field42.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field42.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field42.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field42.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field32
            // 
            this.field32.AcceptsTab = true;
            this.field32.Location = new System.Drawing.Point(72, 64);
            this.field32.MaxLength = 1;
            this.field32.Name = "field32";
            this.field32.Size = new System.Drawing.Size(14, 20);
            this.field32.TabIndex = 21;
            this.field32.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field32.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field32.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field32.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field22
            // 
            this.field22.AcceptsTab = true;
            this.field22.Location = new System.Drawing.Point(52, 64);
            this.field22.MaxLength = 1;
            this.field22.Name = "field22";
            this.field22.Size = new System.Drawing.Size(14, 20);
            this.field22.TabIndex = 20;
            this.field22.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field22.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field22.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field22.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field12
            // 
            this.field12.AcceptsTab = true;
            this.field12.Location = new System.Drawing.Point(32, 64);
            this.field12.MaxLength = 1;
            this.field12.Name = "field12";
            this.field12.Size = new System.Drawing.Size(14, 20);
            this.field12.TabIndex = 19;
            this.field12.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field12.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field12.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field12.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field02
            // 
            this.field02.AcceptsTab = true;
            this.field02.Location = new System.Drawing.Point(12, 64);
            this.field02.MaxLength = 1;
            this.field02.Name = "field02";
            this.field02.Size = new System.Drawing.Size(14, 20);
            this.field02.TabIndex = 18;
            this.field02.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field02.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field02.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field02.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field85
            // 
            this.field85.AcceptsTab = true;
            this.field85.Location = new System.Drawing.Point(172, 142);
            this.field85.MaxLength = 1;
            this.field85.Name = "field85";
            this.field85.Size = new System.Drawing.Size(14, 20);
            this.field85.TabIndex = 53;
            this.field85.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field85.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field85.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field85.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field75
            // 
            this.field75.AcceptsTab = true;
            this.field75.Location = new System.Drawing.Point(152, 142);
            this.field75.MaxLength = 1;
            this.field75.Name = "field75";
            this.field75.Size = new System.Drawing.Size(14, 20);
            this.field75.TabIndex = 52;
            this.field75.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field75.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field75.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field75.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field65
            // 
            this.field65.AcceptsTab = true;
            this.field65.Location = new System.Drawing.Point(132, 142);
            this.field65.MaxLength = 1;
            this.field65.Name = "field65";
            this.field65.Size = new System.Drawing.Size(14, 20);
            this.field65.TabIndex = 51;
            this.field65.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field65.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field65.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field65.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field55
            // 
            this.field55.AcceptsTab = true;
            this.field55.Location = new System.Drawing.Point(112, 142);
            this.field55.MaxLength = 1;
            this.field55.Name = "field55";
            this.field55.Size = new System.Drawing.Size(14, 20);
            this.field55.TabIndex = 50;
            this.field55.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field55.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field55.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field55.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field45
            // 
            this.field45.AcceptsTab = true;
            this.field45.Location = new System.Drawing.Point(92, 142);
            this.field45.MaxLength = 1;
            this.field45.Name = "field45";
            this.field45.Size = new System.Drawing.Size(14, 20);
            this.field45.TabIndex = 49;
            this.field45.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field45.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field45.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field45.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field35
            // 
            this.field35.AcceptsTab = true;
            this.field35.Location = new System.Drawing.Point(72, 142);
            this.field35.MaxLength = 1;
            this.field35.Name = "field35";
            this.field35.Size = new System.Drawing.Size(14, 20);
            this.field35.TabIndex = 48;
            this.field35.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field35.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field35.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field35.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field25
            // 
            this.field25.AcceptsTab = true;
            this.field25.Location = new System.Drawing.Point(52, 142);
            this.field25.MaxLength = 1;
            this.field25.Name = "field25";
            this.field25.Size = new System.Drawing.Size(14, 20);
            this.field25.TabIndex = 47;
            this.field25.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field25.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field25.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field25.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field15
            // 
            this.field15.AcceptsTab = true;
            this.field15.Location = new System.Drawing.Point(32, 142);
            this.field15.MaxLength = 1;
            this.field15.Name = "field15";
            this.field15.Size = new System.Drawing.Size(14, 20);
            this.field15.TabIndex = 46;
            this.field15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field15.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field15.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field15.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field05
            // 
            this.field05.AcceptsTab = true;
            this.field05.Location = new System.Drawing.Point(12, 142);
            this.field05.MaxLength = 1;
            this.field05.Name = "field05";
            this.field05.Size = new System.Drawing.Size(14, 20);
            this.field05.TabIndex = 45;
            this.field05.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field05.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field05.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field05.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field84
            // 
            this.field84.AcceptsTab = true;
            this.field84.Location = new System.Drawing.Point(172, 116);
            this.field84.MaxLength = 1;
            this.field84.Name = "field84";
            this.field84.Size = new System.Drawing.Size(14, 20);
            this.field84.TabIndex = 44;
            this.field84.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field84.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field84.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field84.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field74
            // 
            this.field74.AcceptsTab = true;
            this.field74.Location = new System.Drawing.Point(152, 116);
            this.field74.MaxLength = 1;
            this.field74.Name = "field74";
            this.field74.Size = new System.Drawing.Size(14, 20);
            this.field74.TabIndex = 43;
            this.field74.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field74.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field74.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field74.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field64
            // 
            this.field64.AcceptsTab = true;
            this.field64.Location = new System.Drawing.Point(132, 116);
            this.field64.MaxLength = 1;
            this.field64.Name = "field64";
            this.field64.Size = new System.Drawing.Size(14, 20);
            this.field64.TabIndex = 42;
            this.field64.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field64.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field64.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field64.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field54
            // 
            this.field54.AcceptsTab = true;
            this.field54.Location = new System.Drawing.Point(112, 116);
            this.field54.MaxLength = 1;
            this.field54.Name = "field54";
            this.field54.Size = new System.Drawing.Size(14, 20);
            this.field54.TabIndex = 41;
            this.field54.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field54.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field54.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field54.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field44
            // 
            this.field44.AcceptsTab = true;
            this.field44.Location = new System.Drawing.Point(92, 116);
            this.field44.MaxLength = 1;
            this.field44.Name = "field44";
            this.field44.Size = new System.Drawing.Size(14, 20);
            this.field44.TabIndex = 40;
            this.field44.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field44.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field44.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field44.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field34
            // 
            this.field34.AcceptsTab = true;
            this.field34.Location = new System.Drawing.Point(72, 116);
            this.field34.MaxLength = 1;
            this.field34.Name = "field34";
            this.field34.Size = new System.Drawing.Size(14, 20);
            this.field34.TabIndex = 39;
            this.field34.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field34.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field34.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field34.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field24
            // 
            this.field24.AcceptsTab = true;
            this.field24.Location = new System.Drawing.Point(52, 116);
            this.field24.MaxLength = 1;
            this.field24.Name = "field24";
            this.field24.Size = new System.Drawing.Size(14, 20);
            this.field24.TabIndex = 38;
            this.field24.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field24.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field24.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field24.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field14
            // 
            this.field14.AcceptsTab = true;
            this.field14.Location = new System.Drawing.Point(32, 116);
            this.field14.MaxLength = 1;
            this.field14.Name = "field14";
            this.field14.Size = new System.Drawing.Size(14, 20);
            this.field14.TabIndex = 37;
            this.field14.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field14.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field14.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field14.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field04
            // 
            this.field04.AcceptsTab = true;
            this.field04.Location = new System.Drawing.Point(12, 116);
            this.field04.MaxLength = 1;
            this.field04.Name = "field04";
            this.field04.Size = new System.Drawing.Size(14, 20);
            this.field04.TabIndex = 36;
            this.field04.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field04.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field04.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field04.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field83
            // 
            this.field83.AcceptsTab = true;
            this.field83.Location = new System.Drawing.Point(172, 90);
            this.field83.MaxLength = 1;
            this.field83.Name = "field83";
            this.field83.Size = new System.Drawing.Size(14, 20);
            this.field83.TabIndex = 35;
            this.field83.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field83.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field83.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field83.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field73
            // 
            this.field73.AcceptsTab = true;
            this.field73.Location = new System.Drawing.Point(152, 90);
            this.field73.MaxLength = 1;
            this.field73.Name = "field73";
            this.field73.Size = new System.Drawing.Size(14, 20);
            this.field73.TabIndex = 34;
            this.field73.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field73.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field73.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field73.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field63
            // 
            this.field63.AcceptsTab = true;
            this.field63.Location = new System.Drawing.Point(132, 90);
            this.field63.MaxLength = 1;
            this.field63.Name = "field63";
            this.field63.Size = new System.Drawing.Size(14, 20);
            this.field63.TabIndex = 33;
            this.field63.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field63.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field63.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field63.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field53
            // 
            this.field53.AcceptsTab = true;
            this.field53.Location = new System.Drawing.Point(112, 90);
            this.field53.MaxLength = 1;
            this.field53.Name = "field53";
            this.field53.Size = new System.Drawing.Size(14, 20);
            this.field53.TabIndex = 32;
            this.field53.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field53.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field53.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field53.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field43
            // 
            this.field43.AcceptsTab = true;
            this.field43.Location = new System.Drawing.Point(92, 90);
            this.field43.MaxLength = 1;
            this.field43.Name = "field43";
            this.field43.Size = new System.Drawing.Size(14, 20);
            this.field43.TabIndex = 31;
            this.field43.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field43.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field43.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field43.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field33
            // 
            this.field33.AcceptsTab = true;
            this.field33.Location = new System.Drawing.Point(72, 90);
            this.field33.MaxLength = 1;
            this.field33.Name = "field33";
            this.field33.Size = new System.Drawing.Size(14, 20);
            this.field33.TabIndex = 30;
            this.field33.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field33.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field33.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field33.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field23
            // 
            this.field23.AcceptsTab = true;
            this.field23.Location = new System.Drawing.Point(52, 90);
            this.field23.MaxLength = 1;
            this.field23.Name = "field23";
            this.field23.Size = new System.Drawing.Size(14, 20);
            this.field23.TabIndex = 29;
            this.field23.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field23.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field23.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field23.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field13
            // 
            this.field13.AcceptsTab = true;
            this.field13.Location = new System.Drawing.Point(32, 90);
            this.field13.MaxLength = 1;
            this.field13.Name = "field13";
            this.field13.Size = new System.Drawing.Size(14, 20);
            this.field13.TabIndex = 28;
            this.field13.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field13.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field13.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field13.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field03
            // 
            this.field03.AcceptsTab = true;
            this.field03.Location = new System.Drawing.Point(12, 90);
            this.field03.MaxLength = 1;
            this.field03.Name = "field03";
            this.field03.Size = new System.Drawing.Size(14, 20);
            this.field03.TabIndex = 27;
            this.field03.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field03.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field03.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field03.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field88
            // 
            this.field88.AcceptsTab = true;
            this.field88.Location = new System.Drawing.Point(172, 220);
            this.field88.MaxLength = 1;
            this.field88.Name = "field88";
            this.field88.Size = new System.Drawing.Size(14, 20);
            this.field88.TabIndex = 80;
            this.field88.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field88.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field88.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field88.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field78
            // 
            this.field78.AcceptsTab = true;
            this.field78.Location = new System.Drawing.Point(152, 220);
            this.field78.MaxLength = 1;
            this.field78.Name = "field78";
            this.field78.Size = new System.Drawing.Size(14, 20);
            this.field78.TabIndex = 79;
            this.field78.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field78.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field78.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field78.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field68
            // 
            this.field68.AcceptsTab = true;
            this.field68.Location = new System.Drawing.Point(132, 220);
            this.field68.MaxLength = 1;
            this.field68.Name = "field68";
            this.field68.Size = new System.Drawing.Size(14, 20);
            this.field68.TabIndex = 78;
            this.field68.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field68.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field68.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field68.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field58
            // 
            this.field58.AcceptsTab = true;
            this.field58.Location = new System.Drawing.Point(112, 220);
            this.field58.MaxLength = 1;
            this.field58.Name = "field58";
            this.field58.Size = new System.Drawing.Size(14, 20);
            this.field58.TabIndex = 77;
            this.field58.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field58.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field58.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field58.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field48
            // 
            this.field48.AcceptsTab = true;
            this.field48.Location = new System.Drawing.Point(92, 220);
            this.field48.MaxLength = 1;
            this.field48.Name = "field48";
            this.field48.Size = new System.Drawing.Size(14, 20);
            this.field48.TabIndex = 76;
            this.field48.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field48.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field48.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field48.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field38
            // 
            this.field38.AcceptsTab = true;
            this.field38.Location = new System.Drawing.Point(72, 220);
            this.field38.MaxLength = 1;
            this.field38.Name = "field38";
            this.field38.Size = new System.Drawing.Size(14, 20);
            this.field38.TabIndex = 75;
            this.field38.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field38.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field38.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field38.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field28
            // 
            this.field28.AcceptsTab = true;
            this.field28.Location = new System.Drawing.Point(52, 220);
            this.field28.MaxLength = 1;
            this.field28.Name = "field28";
            this.field28.Size = new System.Drawing.Size(14, 20);
            this.field28.TabIndex = 74;
            this.field28.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field28.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field28.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field28.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field18
            // 
            this.field18.AcceptsTab = true;
            this.field18.Location = new System.Drawing.Point(32, 220);
            this.field18.MaxLength = 1;
            this.field18.Name = "field18";
            this.field18.Size = new System.Drawing.Size(14, 20);
            this.field18.TabIndex = 73;
            this.field18.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field18.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field18.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field18.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field08
            // 
            this.field08.AcceptsTab = true;
            this.field08.Location = new System.Drawing.Point(12, 220);
            this.field08.MaxLength = 1;
            this.field08.Name = "field08";
            this.field08.Size = new System.Drawing.Size(14, 20);
            this.field08.TabIndex = 72;
            this.field08.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field08.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field08.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field08.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field87
            // 
            this.field87.AcceptsTab = true;
            this.field87.Location = new System.Drawing.Point(172, 194);
            this.field87.MaxLength = 1;
            this.field87.Name = "field87";
            this.field87.Size = new System.Drawing.Size(14, 20);
            this.field87.TabIndex = 71;
            this.field87.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field87.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field87.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field87.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field77
            // 
            this.field77.AcceptsTab = true;
            this.field77.Location = new System.Drawing.Point(152, 194);
            this.field77.MaxLength = 1;
            this.field77.Name = "field77";
            this.field77.Size = new System.Drawing.Size(14, 20);
            this.field77.TabIndex = 70;
            this.field77.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field77.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field77.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field77.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field67
            // 
            this.field67.AcceptsTab = true;
            this.field67.Location = new System.Drawing.Point(132, 194);
            this.field67.MaxLength = 1;
            this.field67.Name = "field67";
            this.field67.Size = new System.Drawing.Size(14, 20);
            this.field67.TabIndex = 69;
            this.field67.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field67.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field67.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field67.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field57
            // 
            this.field57.AcceptsTab = true;
            this.field57.Location = new System.Drawing.Point(112, 194);
            this.field57.MaxLength = 1;
            this.field57.Name = "field57";
            this.field57.Size = new System.Drawing.Size(14, 20);
            this.field57.TabIndex = 68;
            this.field57.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field57.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field57.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field57.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field47
            // 
            this.field47.AcceptsTab = true;
            this.field47.Location = new System.Drawing.Point(92, 194);
            this.field47.MaxLength = 1;
            this.field47.Name = "field47";
            this.field47.Size = new System.Drawing.Size(14, 20);
            this.field47.TabIndex = 67;
            this.field47.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field47.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field47.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field47.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field37
            // 
            this.field37.AcceptsTab = true;
            this.field37.Location = new System.Drawing.Point(72, 194);
            this.field37.MaxLength = 1;
            this.field37.Name = "field37";
            this.field37.Size = new System.Drawing.Size(14, 20);
            this.field37.TabIndex = 66;
            this.field37.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field37.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field37.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field37.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field27
            // 
            this.field27.AcceptsTab = true;
            this.field27.Location = new System.Drawing.Point(52, 194);
            this.field27.MaxLength = 1;
            this.field27.Name = "field27";
            this.field27.Size = new System.Drawing.Size(14, 20);
            this.field27.TabIndex = 65;
            this.field27.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field27.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field27.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field27.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field17
            // 
            this.field17.AcceptsTab = true;
            this.field17.Location = new System.Drawing.Point(32, 194);
            this.field17.MaxLength = 1;
            this.field17.Name = "field17";
            this.field17.Size = new System.Drawing.Size(14, 20);
            this.field17.TabIndex = 64;
            this.field17.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field17.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field17.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field17.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field07
            // 
            this.field07.AcceptsTab = true;
            this.field07.Location = new System.Drawing.Point(12, 194);
            this.field07.MaxLength = 1;
            this.field07.Name = "field07";
            this.field07.Size = new System.Drawing.Size(14, 20);
            this.field07.TabIndex = 63;
            this.field07.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field07.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field07.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field07.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field86
            // 
            this.field86.AcceptsTab = true;
            this.field86.Location = new System.Drawing.Point(172, 168);
            this.field86.MaxLength = 1;
            this.field86.Name = "field86";
            this.field86.Size = new System.Drawing.Size(14, 20);
            this.field86.TabIndex = 62;
            this.field86.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field86.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field86.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field86.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field76
            // 
            this.field76.AcceptsTab = true;
            this.field76.Location = new System.Drawing.Point(152, 168);
            this.field76.MaxLength = 1;
            this.field76.Name = "field76";
            this.field76.Size = new System.Drawing.Size(14, 20);
            this.field76.TabIndex = 61;
            this.field76.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field76.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field76.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field76.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field66
            // 
            this.field66.AcceptsTab = true;
            this.field66.Location = new System.Drawing.Point(132, 168);
            this.field66.MaxLength = 1;
            this.field66.Name = "field66";
            this.field66.Size = new System.Drawing.Size(14, 20);
            this.field66.TabIndex = 60;
            this.field66.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field66.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field66.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field66.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field56
            // 
            this.field56.AcceptsTab = true;
            this.field56.Location = new System.Drawing.Point(112, 168);
            this.field56.MaxLength = 1;
            this.field56.Name = "field56";
            this.field56.Size = new System.Drawing.Size(14, 20);
            this.field56.TabIndex = 59;
            this.field56.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field56.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field56.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field56.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field46
            // 
            this.field46.AcceptsTab = true;
            this.field46.Location = new System.Drawing.Point(92, 168);
            this.field46.MaxLength = 1;
            this.field46.Name = "field46";
            this.field46.Size = new System.Drawing.Size(14, 20);
            this.field46.TabIndex = 58;
            this.field46.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field46.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field46.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field46.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field36
            // 
            this.field36.AcceptsTab = true;
            this.field36.Location = new System.Drawing.Point(72, 168);
            this.field36.MaxLength = 1;
            this.field36.Name = "field36";
            this.field36.Size = new System.Drawing.Size(14, 20);
            this.field36.TabIndex = 57;
            this.field36.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field36.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field36.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field36.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field26
            // 
            this.field26.AcceptsTab = true;
            this.field26.Location = new System.Drawing.Point(52, 168);
            this.field26.MaxLength = 1;
            this.field26.Name = "field26";
            this.field26.Size = new System.Drawing.Size(14, 20);
            this.field26.TabIndex = 56;
            this.field26.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field26.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field26.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field26.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field16
            // 
            this.field16.AcceptsTab = true;
            this.field16.Location = new System.Drawing.Point(32, 168);
            this.field16.MaxLength = 1;
            this.field16.Name = "field16";
            this.field16.Size = new System.Drawing.Size(14, 20);
            this.field16.TabIndex = 55;
            this.field16.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field16.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field16.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field16.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // field06
            // 
            this.field06.AcceptsTab = true;
            this.field06.Location = new System.Drawing.Point(12, 168);
            this.field06.MaxLength = 1;
            this.field06.Name = "field06";
            this.field06.Size = new System.Drawing.Size(14, 20);
            this.field06.TabIndex = 54;
            this.field06.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.field06.TextChanged += new System.EventHandler(this.Field_TextChanged);
            this.field06.Validating += new System.ComponentModel.CancelEventHandler(this.Field_Validating);
            this.field06.Validated += new System.EventHandler(this.Field_Validated);
            // 
            // buttonVyresit
            // 
            this.buttonSolve.Location = new System.Drawing.Point(207, 10);
            this.buttonSolve.Name = "buttonVyresit";
            this.buttonSolve.Size = new System.Drawing.Size(75, 23);
            this.buttonSolve.TabIndex = 81;
            this.buttonSolve.Text = "Vyřešit";
            this.buttonSolve.UseVisualStyleBackColor = true;
            this.buttonSolve.Click += new System.EventHandler(this.ButtonSolve_Click);
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(207, 217);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(75, 23);
            this.buttonReset.TabIndex = 82;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.ButtonReset_Click);
            // 
            // SMWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(301, 249);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonSolve);
            this.Controls.Add(this.field88);
            this.Controls.Add(this.field78);
            this.Controls.Add(this.field68);
            this.Controls.Add(this.field58);
            this.Controls.Add(this.field48);
            this.Controls.Add(this.field38);
            this.Controls.Add(this.field28);
            this.Controls.Add(this.field18);
            this.Controls.Add(this.field08);
            this.Controls.Add(this.field87);
            this.Controls.Add(this.field77);
            this.Controls.Add(this.field67);
            this.Controls.Add(this.field57);
            this.Controls.Add(this.field47);
            this.Controls.Add(this.field37);
            this.Controls.Add(this.field27);
            this.Controls.Add(this.field17);
            this.Controls.Add(this.field07);
            this.Controls.Add(this.field86);
            this.Controls.Add(this.field76);
            this.Controls.Add(this.field66);
            this.Controls.Add(this.field56);
            this.Controls.Add(this.field46);
            this.Controls.Add(this.field36);
            this.Controls.Add(this.field26);
            this.Controls.Add(this.field16);
            this.Controls.Add(this.field06);
            this.Controls.Add(this.field85);
            this.Controls.Add(this.field75);
            this.Controls.Add(this.field65);
            this.Controls.Add(this.field55);
            this.Controls.Add(this.field45);
            this.Controls.Add(this.field35);
            this.Controls.Add(this.field25);
            this.Controls.Add(this.field15);
            this.Controls.Add(this.field05);
            this.Controls.Add(this.field84);
            this.Controls.Add(this.field74);
            this.Controls.Add(this.field64);
            this.Controls.Add(this.field54);
            this.Controls.Add(this.field44);
            this.Controls.Add(this.field34);
            this.Controls.Add(this.field24);
            this.Controls.Add(this.field14);
            this.Controls.Add(this.field04);
            this.Controls.Add(this.field83);
            this.Controls.Add(this.field73);
            this.Controls.Add(this.field63);
            this.Controls.Add(this.field53);
            this.Controls.Add(this.field43);
            this.Controls.Add(this.field33);
            this.Controls.Add(this.field23);
            this.Controls.Add(this.field13);
            this.Controls.Add(this.field03);
            this.Controls.Add(this.field82);
            this.Controls.Add(this.field72);
            this.Controls.Add(this.field62);
            this.Controls.Add(this.field52);
            this.Controls.Add(this.field42);
            this.Controls.Add(this.field32);
            this.Controls.Add(this.field22);
            this.Controls.Add(this.field12);
            this.Controls.Add(this.field02);
            this.Controls.Add(this.field81);
            this.Controls.Add(this.field71);
            this.Controls.Add(this.field61);
            this.Controls.Add(this.field51);
            this.Controls.Add(this.field41);
            this.Controls.Add(this.field31);
            this.Controls.Add(this.field21);
            this.Controls.Add(this.field11);
            this.Controls.Add(this.field01);
            this.Controls.Add(this.field80);
            this.Controls.Add(this.field70);
            this.Controls.Add(this.field60);
            this.Controls.Add(this.field50);
            this.Controls.Add(this.field40);
            this.Controls.Add(this.field30);
            this.Controls.Add(this.field20);
            this.Controls.Add(this.field10);
            this.Controls.Add(this.field00);
            this.Name = "SMWindow";
            this.Text = "Sudoku Master";
            this.Load += new System.EventHandler(this.SMWindow_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.SMWindow_Paint);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox field00;
        private System.Windows.Forms.TextBox field10;
        private System.Windows.Forms.TextBox field20;
        private System.Windows.Forms.TextBox field30;
        private System.Windows.Forms.TextBox field40;
        private System.Windows.Forms.TextBox field50;
        private System.Windows.Forms.TextBox field60;
        private System.Windows.Forms.TextBox field70;
        private System.Windows.Forms.TextBox field80;
        private System.Windows.Forms.TextBox field81;
        private System.Windows.Forms.TextBox field71;
        private System.Windows.Forms.TextBox field61;
        private System.Windows.Forms.TextBox field51;
        private System.Windows.Forms.TextBox field41;
        private System.Windows.Forms.TextBox field31;
        private System.Windows.Forms.TextBox field21;
        private System.Windows.Forms.TextBox field11;
        private System.Windows.Forms.TextBox field01;
        private System.Windows.Forms.TextBox field82;
        private System.Windows.Forms.TextBox field72;
        private System.Windows.Forms.TextBox field62;
        private System.Windows.Forms.TextBox field52;
        private System.Windows.Forms.TextBox field42;
        private System.Windows.Forms.TextBox field32;
        private System.Windows.Forms.TextBox field22;
        private System.Windows.Forms.TextBox field12;
        private System.Windows.Forms.TextBox field02;
        private System.Windows.Forms.TextBox field85;
        private System.Windows.Forms.TextBox field75;
        private System.Windows.Forms.TextBox field65;
        private System.Windows.Forms.TextBox field55;
        private System.Windows.Forms.TextBox field45;
        private System.Windows.Forms.TextBox field35;
        private System.Windows.Forms.TextBox field25;
        private System.Windows.Forms.TextBox field15;
        private System.Windows.Forms.TextBox field05;
        private System.Windows.Forms.TextBox field84;
        private System.Windows.Forms.TextBox field74;
        private System.Windows.Forms.TextBox field64;
        private System.Windows.Forms.TextBox field54;
        private System.Windows.Forms.TextBox field44;
        private System.Windows.Forms.TextBox field34;
        private System.Windows.Forms.TextBox field24;
        private System.Windows.Forms.TextBox field14;
        private System.Windows.Forms.TextBox field04;
        private System.Windows.Forms.TextBox field83;
        private System.Windows.Forms.TextBox field73;
        private System.Windows.Forms.TextBox field63;
        private System.Windows.Forms.TextBox field53;
        private System.Windows.Forms.TextBox field43;
        private System.Windows.Forms.TextBox field33;
        private System.Windows.Forms.TextBox field23;
        private System.Windows.Forms.TextBox field13;
        private System.Windows.Forms.TextBox field03;
        private System.Windows.Forms.TextBox field88;
        private System.Windows.Forms.TextBox field78;
        private System.Windows.Forms.TextBox field68;
        private System.Windows.Forms.TextBox field58;
        private System.Windows.Forms.TextBox field48;
        private System.Windows.Forms.TextBox field38;
        private System.Windows.Forms.TextBox field28;
        private System.Windows.Forms.TextBox field18;
        private System.Windows.Forms.TextBox field08;
        private System.Windows.Forms.TextBox field87;
        private System.Windows.Forms.TextBox field77;
        private System.Windows.Forms.TextBox field67;
        private System.Windows.Forms.TextBox field57;
        private System.Windows.Forms.TextBox field47;
        private System.Windows.Forms.TextBox field37;
        private System.Windows.Forms.TextBox field27;
        private System.Windows.Forms.TextBox field17;
        private System.Windows.Forms.TextBox field07;
        private System.Windows.Forms.TextBox field86;
        private System.Windows.Forms.TextBox field76;
        private System.Windows.Forms.TextBox field66;
        private System.Windows.Forms.TextBox field56;
        private System.Windows.Forms.TextBox field46;
        private System.Windows.Forms.TextBox field36;
        private System.Windows.Forms.TextBox field26;
        private System.Windows.Forms.TextBox field16;
        private System.Windows.Forms.TextBox field06;
        private System.Windows.Forms.Button buttonSolve;
        private System.Windows.Forms.Button buttonReset;
    }
}

