﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SudokuMaster
{
    public partial class SMWindow : Form
    {
        SudokuBoard sudoku = new SudokuBoard();

        public SMWindow()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Load data from a board into a form.
        /// </summary>
        private void LoadFormValues()
        {
            ControlCollection ovl =(ControlCollection)this.Controls;
            int ind; // = ovl.IndexOfKey("field22");
            for(int x = 0; x < 9; x++)
            {
                for(int y = 0; y < 9; y++)
                {
                    ind = ovl.IndexOfKey("field" + x + y);
                    ovl[ind].Text = (sudoku.ReadValue(x, y) != 0) ? sudoku.ReadValue(x, y).ToString() : "";
                }
            }
        }

        /// <summary>
        /// enable=false: Disable field edit in GUI.
        /// enable=true:  Enable field edit in GUI.
        /// </summary>
        /// <param name="enable"></param>
        private void DisableEdit(bool enable=false)
        {
            ControlCollection ovl = (ControlCollection)this.Controls;
            int ind;
            TextBox tb;
            for (int x = 0; x < 9; x++)
            {
                for (int y = 0; y < 9; y++)
                {
                    ind = ovl.IndexOfKey("field" + x + y);
                    tb = (TextBox)ovl[ind];
                    tb.ReadOnly = (enable) ? false : true;
                    tb.TabStop = (enable) ? true : false;
                }
            }
        }

        /// <summary>
        /// Save a form data into the board.
        /// </summary>
        private void SaveFormValues()
        {
            ControlCollection ovl = (ControlCollection)this.Controls;
            int ind;
            string loadb = "";
            for (int y = 0; y < 9; y++)
            {
                for (int x = 0; x < 9; x++)
                {
                    ind = ovl.IndexOfKey("field" + x + y);
                    loadb += (ovl[ind].Text.Length != 0) ? ovl[ind].Text.Substring(0, 1) : ".";
                }
            }
            sudoku.LoadBoard(loadb);
            
        }

        /// <summary>
        /// Test if value added follow the rules.
        /// </summary>
        /// <param name="x">Coord X (0-8)</param>
        /// <param name="y">Coord Y (0-8)</param>
        /// <param name="value">The value checked.</param>
        /// <returns>True - ok, False - not ok.</returns>
        private bool CheckFieldValue(int x, int y, string value)
        {
            if (value == "") return true;                  //Empty is valid.
            if (value.Length > 1) return false;            //More then one character is not plausible (this condition is addressed in properties too)

            int ind, count = 0;
            ControlCollection ovl = (ControlCollection)this.Controls;
            if (!"123456789".Contains(value))
            {
                ind = ovl.IndexOfKey("field" + x + y);
                ovl[ind].Text = "";

                return true; //Non digit equals empty, allways valid.
            }

            for(int a = 0; a < 9; a++)
            {
                ind = ovl.IndexOfKey("field" + a + y);
                if (ovl[ind].Text == value) count++;
                ind = ovl.IndexOfKey("field" + x + a);
                if (ovl[ind].Text == value) count++;
            }

            int ax = (x / 3) * 3;
            int bx = (x / 3 + 1) * 3;
            int ay = (y / 3) * 3;
            int by = (y / 3 + 1) * 3;

            for (int cx = ax; cx < bx; cx++)
            {
                for (int cy = ay; cy < by; cy++)
                {
                    ind = ovl.IndexOfKey("field" + cx + cy);
                    if (ovl[ind].Text == value) count++;
                }
            }

            if (count == 3) return true;
            return false;
        }

        private void SMWindow_Load(object sender, EventArgs e)
        {
            LoadFormValues();
        }

        /// <summary>
        /// Draws a grid and undercolor for the courts.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SMWindow_Paint(object sender, PaintEventArgs e)
        {
            Graphics paper = e.Graphics;
            Pen myPen = new Pen(Color.Black, 2);
            var myBrush = Brushes.Aquamarine;
            paper.FillRectangle(myBrush, 8, 8, 61, 79);
            paper.FillRectangle(myBrush, 129, 8, 61, 79);
            paper.FillRectangle(myBrush, 69, 87, 61, 79);
            paper.FillRectangle(myBrush, 8, 165, 61, 79);
            paper.FillRectangle(myBrush, 129, 165, 61, 79);
            paper.DrawRectangle(myPen, 8, 8, 181, 235);
            paper.DrawLine(myPen, 8, 87, 190, 87);
            paper.DrawLine(myPen, 8, 165, 190, 165);
            paper.DrawLine(myPen, 69, 8, 69, 244);
            paper.DrawLine(myPen, 129, 8, 129, 244);
        }

        private void Field_TextChanged(object sender, EventArgs e)
        {
            Control send = (Control)sender;

            if (send.Text.Length == 1) SelectNextControl(send, true, true, false, true);
        }

        private void Field_Validating(object sender, CancelEventArgs e)
        {
            Control ctr = (Control)sender;
            string name=ctr.Name;
            int x = Convert.ToInt32(name.Substring(name.Length-2,1));
            int y = Convert.ToInt32(name.Substring(name.Length-1,1));
            bool ok = CheckFieldValue(x, y, ctr.Text);
            if (!ok)
            {
                ctr.BackColor = Color.LightPink;
                e.Cancel=true;
            }
            else
            {
                e.Cancel = false;
            }
        }

        private void ButtonSolve_Click(object sender, EventArgs e)
        {
            DisableEdit();
            SaveFormValues();
            sudoku.BoardSolver();
            sudoku.WriteBoard("-GUI");//Zapíše řešení do souboru (ladění)
            LoadFormValues();
        }

        private void ButtonReset_Click(object sender, EventArgs e)
        {
            sudoku.ResetBoard();
            LoadFormValues();
            DisableEdit(true);
        }

        private void Field_Validated(object sender, EventArgs e)
        {
            var send=(Control)sender;
            var tb = (TextBox)sender;
            send.BackColor = (tb.ReadOnly) ? this.BackColor : Color.White;
        }
    }
}
