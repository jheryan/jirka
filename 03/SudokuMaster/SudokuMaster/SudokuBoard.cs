﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuMaster
{
    public class SudokuBoard
    {
        ushort[,] board = new ushort[9, 9]; //Sudoku numbers field
        bool[,] immutable = new bool[9, 9]; //True - immutable digit added from task
        string[,] possibleChoices = new string[9, 9]; //String contains digit choices for the possition

        //Debug + analysis
        int recursesUsed=0; //No. of recurse calls from the previous write
        int recurseLevel = 0; //Maximal BoardSolver method recurse level.
        string board_origin; //Saved task string
        

        public SudokuBoard()
        {
            ResetBoard();
        }

        public void ResetBoard()
        {
            for (int x = 0; x < 9; x++)
            {
                for (int y = 0; y < 9; y++)
                {
                    board[x, y] = (ushort)0;
                    immutable[x, y] = false;
                    possibleChoices[x, y] = "123456789";
                }
            }

        }

        /// <summary>
        /// Inserts the digit into the field with respect to all sudoku rules. Changes possible choices in
        /// affected fields. If hard is true, sets the field immutable.
        /// </summary>
        /// <param name="x">column</param>
        /// <param name="y">row</param>
        /// <param name="value">value</param>
        /// <param name="hard">immutability</param>
        /// <returns></returns>
        public bool SetMember(int x, int y, ushort value, bool hard = false)
        {
            string oldnum = board[x, y].ToString();
            if (possibleChoices[x, y].Contains(value.ToString()))
            {
                board[x, y] = value;
                immutable[x, y] = hard;
                if (hard) possibleChoices[x, y] = "";
                EditRowAndColumnChoices(x, y, value, oldnum);
                EditSquareCourtChoices(x, y, value, oldnum);
                return true;
            }
            return false;
        }

        private void EditRowAndColumnChoices(int x, int y, ushort value, string oldnum)
        {
            for (int z = 0; z < 9; z++)
            {
                if (oldnum != "0")
                {
                    if (!immutable[z, y]) possibleChoices[z, y] += oldnum;
                    if (!immutable[x, z]) possibleChoices[x, z] += (y == z) ? "" : oldnum; //On x,y I want one time only addition
                }
                possibleChoices[z, y] = possibleChoices[z, y].Replace(value.ToString(), "");
                possibleChoices[x, z] = possibleChoices[x, z].Replace(value.ToString(), "");
            }
        }

        private void EditSquareCourtChoices(int x, int y, ushort value, string oldnum)
        {
            int ax = (x / 3) * 3;
            int bx = (x / 3 + 1) * 3;
            int ay = (y / 3) * 3;
            int by = (y / 3 + 1) * 3;

            for (int cx = ax; cx < bx; cx++)
            {
                for (int cy = ay; cy < by; cy++)
                {
                    if (oldnum != "0" && !immutable[cx, cy])
                    {
                        possibleChoices[cx, cy] += (cx == x && cy == y) ? "" : oldnum;
                    }
                    possibleChoices[cx, cy] = possibleChoices[cx, cy].Replace(value.ToString(), "");
                }
            }
        }

        /// <summary>
        /// Loads a sudoku task into a matrix. The task is a string containing a sudoku task, where every chracter
        /// represents a single field of sudoku. The non digits and zero reprezents an empty field.
        /// The method raise an exception, if some of digits violate sudoku rules.
        /// </summary>
        /// <param name="task"></param>
        public void LoadBoard(string task)
        {
            board_origin = task; //Save task
            ResetBoard(); //Empty the board
            if (task.Length > 81) task = task.Substring(0, 81);
            bool ok = true;
            int position = 0,x,y,digit;
            foreach(char num in task)
            {
                if (Char.IsNumber(num))
                {
                    digit = Convert.ToInt32(num.ToString());
                    x = position % 9;
                    y = position / 9;
                    if ("123456789".Contains(num))
                    {
                        ok = SetMember(x, y, (ushort)digit, true);
                    }
                    if (!ok) throw new Exception("Error: The number " + num + " on position " + x + "," + y + " invalidate rules.");
                }
                position++;
            }
        }

        /// <summary>
        /// The method attempts to solve the board.
        /// It is recurse called untill the solution is fond or definitely impossible.
        /// </summary>
        /// <returns>True if succeed.</returns>
        public bool BoardSolver(int recurse=0)
        {
            //Debug info
            recursesUsed++;
            if (recurseLevel < recurse) recurseLevel = recurse;
            //---

            if (recurse > 80) throw new Exception("Critical: Maximum recurse level reached!");

            if (HeuristicMethodSolver()) return true;

            return RecursiveTryAndSeeSolver(recurse);
        }

        /// <summary>
        /// Method iterate board, finding a field with the shortest choices list.
        /// If the combination of empty field (0) and empty choices list occurs, it raise "No solution this way." exception.
        /// Method out return a string copy of actual board values (here for performance reason)
        /// </summary>
        /// <returns>int[3] array with coordinates and the lists' length</returns>
        private int[] FindShortestChoiceList(out string boardCopy)
        {
            int x, y, shortestLength=10;
            boardCopy = "";
            for (int i = 0; i < 81; i++)
            {
                x = i % 9;
                y = i / 9;
                boardCopy += ReadValue(x, y);
                if (ReadValue(x, y) == 0 && ReadChoices(x, y).Length == 0) throw new Exception("No solution this way.");
                if (ReadChoices(x, y).Length > 0 && ReadChoices(x, y).Length < shortestLength)
                {
                    return new int[] { x, y, ReadChoices(x, y).Length};
                }
            }
            return new int[] { 0, 0, 0 };
        }

        /// <summary>
        /// Method tries list of choices and recursively calls the BoardSolver() to find a solution.
        /// Can raise the "Error: Recursive method tried invalid choice! ..." exception.
        /// </summary>
        /// <param name="recurse"></param>
        /// <returns>False means no solution was fond. True means success.</returns>
        private bool RecursiveTryAndSeeSolver(int recurse)
        {
            int[] shortest;
            string saveboard = "";// SaveBoard();
            try
            {
                shortest = FindShortestChoiceList(out saveboard);
            }
            catch
            {
                return false;
            }

            string choices = ReadChoices(shortest[0], shortest[1]);
            foreach (char choice in choices)
            {
                if (SetMember(shortest[0], shortest[1], (ushort)Convert.ToUInt16(choice.ToString()), true))
                {
                    //The choice is inserted
                    if (BoardSolver(recurse + 1)) return true;
                }
                else
                {
                    throw new Exception("Error: Recursive method tried invalid choice! :" + shortest[0] + ", " + shortest[1] + ": " + choice);
                }
                ResetBoard();
                LoadBoard(saveboard);
            }

            return false; //Fail if no other option
        }

        /// <summary>
        /// Method'll fill the fields with only choice left until it can't find any or the board is solved.
        /// </summary>
        /// <returns>True when the board is solved. False otherways.</returns>
        private Boolean HeuristicMethodSolver()
        {
            int x, y;
            int position = 0;
            int inserts = 0;
            int zeroValueCount = 0;
            bool inserted;
            while (true)
            {
                x = position % 9;
                y = position / 9;
                if (possibleChoices[x, y].Length == 1)
                {
                    inserted = SetMember(x, y, Convert.ToUInt16(possibleChoices[x, y]));
                    if (inserted) inserts++;
                }
                position++;
                if (ReadValue(x, y) == 0) zeroValueCount++;
                if (x == 8 && y == 8)
                {
                    position = 0;
                    if (zeroValueCount == 0) return true;
                    zeroValueCount = 0;
                    if (inserts == 0) break;
                    inserts = 0;
                }
            }
            return false;
        }

        //Data read handlers
        public string ReadChoices(int x,int y)
        {
            return possibleChoices[x, y];
        }
        public bool ReadImmutable(int x, int y)
        {
            return immutable[x, y];
        }
        public ushort ReadValue(int x, int y)
        {
            return board[x, y];
        }

        //Write to file (debug)
        /// <summary>
        /// Method writes the solution of board into a Sudoku_Board_values<suffix>.txt file.
        /// Intended for debuging.
        /// </summary>
        /// <param name="suffix">filename suffix</param>
        public void WriteBoard(string suffix="")
        {
            TextWriter s = File.CreateText("Sudoku_Board_values" + suffix + ".txt");

            ///*
            s.WriteLine("Sudoku board:");
            s.WriteLine("Task: " + board_origin);
            s.WriteLine("Solution:");
            string line = "", line2 = "";
            for (int a = 0; a < 9; a++)
            {
                for (int b = 0; b < 9; b++)
                {
                    line += " " + ReadValue(b, a).ToString();
                    line2+= " (" + ReadChoices(b, a).Length + ")";
                }
                //s.WriteLine(line+" |"+line2);
                s.WriteLine(line);
                line = "";
                line2 = "";
            }
            s.WriteLine("BoardSolver calls count:" + recursesUsed + "   Maximum recurse level:" + recurseLevel);
            //*/  
            //s.WriteLine("Shortest:" + shortest[0] + "," + shortest[1] + " length:" + shortest[2]);
            s.Close();
            recurseLevel = 0;
            recursesUsed = 0;

        }
    }
}
