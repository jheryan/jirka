﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SudokuMaster;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SudokuMaster.Tests
{
    [TestClass()]
    public class SudokuBoardTests
    {
        [TestMethod()]
        public void SudokuBoardTest()
        {
            SudokuBoard sudoku = new SudokuBoard();
            Assert.AreEqual(0, sudoku.ReadValue(2, 2));
            Assert.AreEqual("123456789", sudoku.ReadChoices(2, 2));
        }

        [TestMethod()]
        public void setMemeberTest()
        {
            //Tests: insert, not insert invalid and reduce options for the row and the column
            SudokuBoard sudoku = new SudokuBoard();
            sudoku.SetMember(2, 3, 5, true);
            sudoku.SetMember(3, 2, 4);
            Assert.AreEqual("1236789", sudoku.ReadChoices(3, 3));
            Assert.AreEqual("12346789", sudoku.ReadChoices(2, 8));
            Assert.AreEqual("12356789", sudoku.ReadChoices(8, 2));
            sudoku.SetMember(3, 4, 4);
            Assert.AreEqual(0, sudoku.ReadValue(3, 4)); //Test valid for not inserted
            sudoku.SetMember(3, 2, 2);
            sudoku.SetMember(1, 5, 4);
            sudoku.SetMember(1, 5, 3);
            Assert.AreEqual(2, sudoku.ReadValue(3, 2)); //Tests valid if value changed
            Assert.AreEqual("13567894", sudoku.ReadChoices(3, 2));//and choices was correctly reduced
            Assert.AreEqual("13567894", sudoku.ReadChoices(3, 4));
            Assert.AreEqual("13567894", sudoku.ReadChoices(5, 0));
            Assert.AreEqual("", sudoku.ReadChoices(2, 3));
        }

        [TestMethod()]
        public void setMemeberTest2()
        {
            //Test of choices reduction in square courts
            SudokuBoard sudoku = new SudokuBoard();
            sudoku.SetMember(0, 0, 1);
            Assert.AreEqual("23456789", sudoku.ReadChoices(2, 2));
            sudoku.SetMember(1, 1, 2);
            Assert.AreEqual("3456789", sudoku.ReadChoices(2, 1));
            sudoku.SetMember(0, 3, 3);
            Assert.AreEqual("12456789", sudoku.ReadChoices(2, 4));
            sudoku.SetMember(6, 6, 9);
            Assert.AreEqual("12345678", sudoku.ReadChoices(8, 8));
        }

        [TestMethod()]
        public void loadBoardTest()
        {
            //Test of board loading
            SudokuBoard sudoku = new SudokuBoard();
            Assert.AreEqual("123456789", sudoku.ReadChoices(0, 0));
            sudoku.LoadBoard("350004600000007140910060720470100002093020450800003016049080071081500000002700084");
            Assert.AreEqual(5, sudoku.ReadValue(1, 0));
            Assert.AreEqual(8, sudoku.ReadValue(4, 6));
            Assert.AreEqual(4, sudoku.ReadValue(8, 8));
        }

        [TestMethod()]
        public void loadBoardTest2()
        {
            //Test loading not fully correct board (string length, use of letters for empty)
            SudokuBoard sudoku = new SudokuBoard();
            Assert.AreEqual("123456789", sudoku.ReadChoices(0, 0));
            sudoku.LoadBoard("35000460000000714091006072047010000209302045080h00301604908007108150000000270008451");
            Assert.AreEqual(4, sudoku.ReadValue(8, 8));
        }

        [TestMethod()]
        public void resitelTest()
        {
            //Tests with results writen to file(s). Check the file Sudoku_board...txt (see SudokuBoard.WriteBoard)
            SudokuBoard sudoku = new SudokuBoard();

            sudoku.LoadBoard("...4...57....17..6..5...8......9...17....3..4..8..5...65.83..7..1...6....9..7....");//319
            //sudoku.BoardSolver();
            Assert.IsTrue(sudoku.BoardSolver());
            sudoku.WriteBoard("319");
            
            sudoku.ResetBoard();
            sudoku.LoadBoard("3,,,6,,,2,,,9,,,,88,52,,47,,7,,,,,,,1,8,,,6,7,,9,57,1,,,,,1,,,,,,76,,,,,,6,,,,,,3");//320
            Assert.IsTrue(sudoku.BoardSolver());
            sudoku.WriteBoard("320");

            sudoku.ResetBoard();
            sudoku.LoadBoard(",,,16,,3,,,,,8,5,9,58,,,,7,9,,,,,,,,,,183,,,,,,4,,,,52,,,,4,6,7,6,,,12,,8,,,,,,,1");//323
            Assert.IsTrue(sudoku.BoardSolver());
            sudoku.WriteBoard("323");
            
        }
    }
}