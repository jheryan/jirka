﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struktura
{
    class Program
    {
        static void Main(string[] args)
        {
            Datamodel data = new Datamodel();
            data.addMemeber("Jaromír Heryán", -1);
            foreach(string name in new string[] { "Jiří Heryán", "Lukáš Heryán", "Bohuslava Heryánová" })
            {
                data.addMemeber(name, data.getId("Jaromír Heryán"));
            }
            foreach (string name in new string[] { "Ondřej Heryán", "Leontýna Heryánová" })
            {
                data.addMemeber(name, data.getId("Jiří Heryán"));
            }
            foreach (string name in new string[] { "Šimon Heryán", "Markéta Heryánová", "Josef Heryán" })
            {
                data.addMemeber(name, data.getId("Lukáš Heryán"));
            }

            //Data nakrmeny, testujeme
            Console.WriteLine("Zadej jméno osoby:");
            while (true)
            {
                string a = Console.ReadLine();
                if (a == "") break;
                Console.WriteLine(data.getRod(a));
            }
            Console.WriteLine("---enter=konec---");
            Console.ReadLine();
        }
    }
}
