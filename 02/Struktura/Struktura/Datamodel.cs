﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Struktura
{
    public class Datamodel
    {
        int currentId = 0;
        public struct Rodina
        {
            internal int id;      //Identifikátor
            internal string name; //Jméno
            internal int rodic;   //ID rodiče

            public Rodina(int iid,string iname,int irodic)
            {
                id = iid;
                name = iname;
                rodic = irodic;
            }
        }
        public SortedList tabulka = new SortedList();

        /// <summary>
        /// Atribut jen pro čtení. Vrací aktuální volné ID a zvýší jej o 1.
        /// </summary>
        public int CurrentId
        {
            get
            {
                return currentId++;
            }
        }

        /// <summary>
        /// Přidá záznam typu Rodina do sorted listu. Automaticky přidělí ID.
        /// </summary>
        /// <param name="name">string "Jméno"</param>
        /// <param name="parent">int "ID rodiče"</param>
        /// <returns></returns>
        public void addMemeber(string name, int parent)
        {
            int id = getId(name);
            if (id != -1)
            {
                tabulka.Remove(getId(name));
            }
            else
            {
                id = CurrentId;
            }
            if (id == parent) parent = -1; //Ošetření možné rekurze
            tabulka.Add(id, new Rodina(id, name, parent));
        }

        /// <summary>
        /// Vrátí ID osoby zadaného jména
        /// </summary>
        /// <param name="jmeno">Jméno ososby</param>
        /// <returns>ID osoby</returns>
        public int getId(string jmeno)
        {
            for (int i=0; i<tabulka.Count; i++)
            {
                Rodina j = (Rodina) tabulka.GetByIndex(i);
                if (j.name == jmeno) return j.id;
            }
            return -1;
        }

        /// <summary>
        /// Vrátí rod zadané osoby ve tvaru: (...) děd->otec->zadaný potomek.
        /// </summary>
        /// <param name="member"></param>
        /// <returns></returns>
        public string getRod(string member)
        {
            int index = getId(member);
            if (index < 0) return "---záznam neexistuje---"; //Pokud hodnota neexistuje vracíme "---záznam neexistuje---".
            string vystup = member;
            Rodina zaznam = (Rodina)tabulka.GetByIndex(index);
            int rodic = zaznam.rodic;
            while (tabulka.ContainsKey(rodic))
            {
                zaznam = (Rodina)tabulka.GetByIndex(rodic);
                vystup = zaznam.name + "->" + vystup;
                rodic = (rodic == zaznam.rodic) ? -1 : zaznam.rodic;
            }
            return vystup;
        }
    }
}
