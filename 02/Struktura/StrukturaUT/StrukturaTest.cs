﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Struktura;

namespace StrukturaUT
{
    [TestClass]
    public class StrukturaTest
    {
        [TestMethod]
        public void addMemeberTest()
        {
            //Test funkčnosti datové struktury a schopnosti zadávání dat
            Datamodel data = new Datamodel();
            data.addMemeber("Zkouska", -1);
            int length = data.tabulka.Count;
            Assert.AreEqual(1, length); //Byl vložen záznam?
            data.addMemeber("Test", -1);
            Assert.AreEqual(true, data.tabulka.ContainsKey(0));//Existuje první index?
            Assert.AreEqual(true, data.tabulka.ContainsKey(1));//Dochází k navyšování generátoru indexu?
        }

        [TestMethod]
        public void getIdTest()
        {
            //Test funkce getId, která má vracet ID zadaného člena struktury
            Datamodel data = new Datamodel();
            data.addMemeber("jedna", -1);
            data.addMemeber("dve", -1);
            data.addMemeber("tri", -1);
            Assert.AreEqual(2, data.getId("tri"));
            Assert.AreEqual(1, data.getId("dve"));
            Assert.AreEqual(0, data.getId("jedna"));
            Assert.AreEqual(-1, data.getId("ctyri"));//Neexistující hodnota
            Assert.AreEqual(-1, data.getId("pet"));//Neexistující hodnota
        }

        [TestMethod]
        public void getRodTest()
        {
            //Test funkce vracející strom rodičů ve tvaru děd->otec->syn atd.
            Datamodel data = new Datamodel();
            data.addMemeber("jedna", -1);
            data.addMemeber("dve", data.getId("jedna"));
            data.addMemeber("tri", -1);
            Assert.AreEqual("jedna->dve", data.getRod("dve"));
            Assert.AreEqual("tri", data.getRod("tri"));
            Assert.AreEqual("---záznam neexistuje---", data.getRod("neexistuje"));
            data.addMemeber("tri", 0); //Přepíše stávající člen
            Assert.AreEqual("jedna->tri", data.getRod("tri"));
        }

        [TestMethod]
        public void getRodTest2()
        {
            //Test rekurzivního zaseknutí
            //??? Nevím jak vymezit jen určitý čas na provedení kódu v případě zacyklení ???
            Datamodel data = new Datamodel();
            data.addMemeber("jedna", 0); //Rodič = potomek = rekurze
            Assert.AreEqual(0, data.getId("jedna"));
            Assert.AreEqual("jedna", data.getRod("jedna"));
        }

    }
}
