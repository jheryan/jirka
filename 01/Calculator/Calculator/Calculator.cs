﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalculatorNS
{
    public class Calculator
    {
        private double pamet;
        private string operace;

        public Calculator()
        {
            Console.WriteLine("Vietnamska kalkulačka. Podporujeme jen '+-/*'. Prázdný řádek=konec.");
            operace = "";
            input("0");
        }
        public double Pamet
        {
            set
            {
                pamet = value;
            }
            get
            {
                return pamet;
            }
        }

        /// <summary>
        /// Vynásobí paměť kalkulačky parametrem číslo.
        /// </summary>
        /// <param name="cislo">Násobitel</param>
        /// <returns>Násobek.</returns>
        public double multiply(double cislo)
        {
            return pamet * cislo;
        }

        /// <summary>
        /// Vydělí paměť kalkulačky parametrem číslo.
        /// </summary>
        /// <param name="cislo">Dělitel</param>
        /// <returns>Podíl.</returns>
        public double divide(double cislo)
        {
            if (cislo == 0) throw new System.DivideByZeroException();
            return pamet / cislo;
        }

        /// <summary>
        /// Sečte paměť kalkulačky s číslem.
        /// </summary>
        /// <param name="cislo"></param>
        /// <returns>Součet.</returns>
        public double add(double cislo)
        {
            return pamet + cislo;
        }

        /// <summary>
        /// Odečte číslo od paměti kalkulačky.
        /// </summary>
        /// <param name="cislo"></param>
        /// <returns>Rozdíl.</returns>
        public double sub(double cislo)
        {
            return pamet - cislo;
        }

        /// <summary>
        /// Zpracuje vstup kalkulátoru a provede požadovanou akci.
        /// </summary>
        /// <param name="vstup"></param>
        public void input(string vstup)
        {
            try
            {
                double cislo = Convert.ToDouble(vstup);
                if (operace == "")
                {
                    pamet = cislo;
                }
                else
                {
                    switch(operace)
                    {
                        case "+":
                            operace = "";
                            pamet = add(cislo);
                            break;
                        case "-":
                            operace = "";
                            pamet = sub(cislo);
                            break;
                        case "*":
                            operace = "";
                            pamet = multiply(cislo);
                            break;
                        case "/":
                            operace = "";
                            pamet = divide(cislo);
                            break;
                        case "=":
                            operace = "";
                            break;
                        default:
                            Console.WriteLine("---nepodporovaná operace '{0}' neprovedena---",operace.ToString());
                            pamet = cislo;
                            break;
                    }
                }
            }
            catch (DivideByZeroException)
            {
                Console.WriteLine("Chyba: nulou nelze dělit.");
                pamet = 0;
            }
            catch
            {
                //Vstupem není číslo. Operace?
                if (vstup.Length==1)
                {
                    operace = vstup;
                }
                else
                {
                    Console.WriteLine("Chyba vstupu: Zadejte číslo nebo početní operaci (+-/*).");
                }
            }
            Console.WriteLine(">>: {0} {1}", pamet.ToString(),operace);
        }
    }
}
