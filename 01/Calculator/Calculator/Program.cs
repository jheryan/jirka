﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalculatorNS;

namespace CalculatorNS
{
    class Program
    {
        static void Main(string[] args)
        {
            Calculator calc = new Calculator();
            while (true)
            {
                string input = Console.ReadLine();
                if (input == "") break;
                calc.input(input);
            }
            
        }
    }
}
