﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CalculatorNS;

namespace CalculatorTests
{
    [TestClass]
    public class BasicFunctionTests
    {
        [TestMethod]
        public void inputTest1()
        {
            //Provede sérii simulovaných výpočtů a vyhodnotí výsledek
            Calculator calc = new Calculator();
            calc.input("200");
            calc.input("+");
            calc.input("100");
            calc.input("/");
            calc.input("2");
            double expected = 150;
            Assert.AreEqual(expected, calc.Pamet, 0.00000000001);
        }

        [TestMethod]
        public void inputTest2()
        {
            //Provete test dělení nulou
            Calculator calc = new Calculator();
            calc.input("200");
            calc.input("/");
            calc.input("0");
            Assert.AreEqual(0, calc.Pamet, 0.0000000001);
        }

        [TestMethod]
        public void inputTest3()
        {
            //Provete test uložení čísla do kalkulačky
            Calculator calc = new Calculator();
            calc.input("200");
            Assert.AreEqual(200, calc.Pamet, 0.0000000001);
        }

        [TestMethod]
        public void additionTest1()
        {
            //Test1 - sčítání kladných
            Calculator calc = new Calculator();
            calc.Pamet = 0.33333;
            double cislo = 1;
            calc.Pamet = calc.add(cislo);
            double expected = 1.33333;
            Assert.AreEqual(expected, calc.Pamet, 0.000000000000001);
        }

        [TestMethod]
        public void additionTest2()
        {
            //Test2 - sčítání kladného se záporným
            Calculator calc = new Calculator();
            calc.Pamet = 1.33333;
            double expected = -0.66667;
            double cislo = -2;
            calc.Pamet = calc.add(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.000000000000001);
        }

        [TestMethod]
        public void additionTest3()
        {
            //Test3 - sčítání záporného se záporným
            Calculator calc = new Calculator();
            calc.Pamet = -0.66667;
            double cislo = -2;
            double expected = -2.66667;
            calc.Pamet = calc.add(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.000000000000001);
        }

        [TestMethod]
        public void substitutionTest1()
        {
            //Test1 - odčítání kladných
            Calculator calc = new Calculator();
            calc.Pamet = 0.33333;
            double cislo = 1;
            calc.Pamet = calc.sub(cislo);
            double expected = -0.66667;
            Assert.AreEqual(expected, calc.Pamet, 0.000000000000001);
        }

        [TestMethod]
        public void substitutionTest2()
        {
            //Test2 - odčítání záporného od kladného
            Calculator calc = new Calculator();
            calc.Pamet = 1.33333;
            double expected = 3.33333;
            double cislo = -2;
            calc.Pamet = calc.sub(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.000000000000001);
        }

        [TestMethod]
        public void substitutionTest3()
        {
            //Test3 - odčítání záporného od záporného
            Calculator calc = new Calculator();
            calc.Pamet = -0.66667;
            double cislo = -2;
            double expected = 1.33333;
            calc.Pamet = calc.sub(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.000000000000001);
        }

        [TestMethod]
        public void multiplicationTest1()
        {
            //Test1 - násobení kladných
            Calculator calc = new Calculator();
            calc.Pamet = 25;
            double cislo = 5.336;
            double expected = 133.4;
            calc.Pamet = calc.multiply(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.000000000000001);
        }

        [TestMethod]
        public void multiplicationTest2()
        {
            //Test2 - násobení kladného záporným
            Calculator calc = new Calculator();
            calc.Pamet = 25;
            double cislo = -5.336;
            double expected = -133.4;
            calc.Pamet = calc.multiply(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.000000000000001);
        }

        [TestMethod]
        public void multiplicationTest3()
        {
            //Test1 - násobení nulou
            Calculator calc = new Calculator();
            calc.Pamet = 25;
            double cislo = 0;
            double expected = 0;
            calc.Pamet = calc.multiply(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.000000000000001);
        }

        [TestMethod]
        public void multiplicationTest4()
        {
            //Test1 - násobení záporných
            Calculator calc = new Calculator();
            calc.Pamet = -25.8975;
            double cislo = (double) -8/3;
            double expected = 69.06;
            calc.Pamet = calc.multiply(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.000000000000001);
        }
        [TestMethod]
        public void divisionTest1()
        {
            //Test1 - dělení kladných
            Calculator calc = new Calculator();
            calc.Pamet = 25;
            double cislo = 5.336;
            double expected = 4.68515742129;
            calc.Pamet = calc.divide(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.0000000001);
        }

        [TestMethod]
        public void divisionTest2()
        {
            //Test2 - dělení kladného záporným
            Calculator calc = new Calculator();
            calc.Pamet = 25;
            double cislo = -5.336;
            double expected = -4.68515742129;
            calc.Pamet = calc.divide(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.0000000001);
        }

        [TestMethod, ExpectedException(typeof(DivideByZeroException))]
        public void divisionTest3()
        {
            //Test3 - dělení nulou
            Calculator calc = new Calculator();
            calc.Pamet = 25;
            double cislo = 0;
            calc.divide(cislo);
        }

        [TestMethod]
        public void divisionTest4()
        {
            //Test4 - dělení záporných
            Calculator calc = new Calculator();
            calc.Pamet = -25.8975;
            double cislo = (double)-8 / 3;
            double expected = 9.7115625;
            calc.Pamet = calc.divide(cislo);
            Assert.AreEqual(expected, calc.Pamet, 0.00000000000001);
        }

    }
}
