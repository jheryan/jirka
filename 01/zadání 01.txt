﻿Zadání úkolu č.1

Vytvoříš konzolovou kalkulačku.
Bude umět sčítat, odečítat, násobit, dělit
Výpočetní funkce budou otestovány unittestem! 
Projekt uložíš do tohoto adresáře.
Očekávám tedy 2 projekty: Calculator a CalculatorUT

diskuze:
Testování správné funkce programu testuj v unittestu. Konzoli nepotřebuješ.
Až ti pojedou výpočtové funkce, doděláš si vstupy přes konzoli. Pravděpodobně na to budeš mít zase nějakou funkci, která ti zpracuje vtupní řetězec a vyhodnotí co v něm je.
Opět otestuješ správnou funkci unittestem. Konzoli nepotřebuješ :-)
Na závěr zpracuješ konzolový vstup. Jak vidíš, GUI potřebuješ až úplně na závěr.

Toto jsou správné programátorské návyky. Odhalí se tím chyby ještě dříve než vzniknou a zároveň tě to nutí správně strukturovat kód. Pokud bys začal vstupem z konzole a ten dál zpracoval, udělal výpočet, výsledek vypsal v konzoli, tak ti to bude fungovat, ale budeš muset ručně testovat všechny kombinace a v případě chyby zkoumat, kde je co špatně (výpočet?, zpracování textu?, konzole?). Takto jakmile budeš mít jednou test napsaný, už se program testuje sám. Nemusíš si pamatovat co kde jak nastavit apod. v případě tohoto primitivního porgramu to vypadá úsměvně, ale jak programy bobtnají tak te práce s testováním tam zpět, jestli někde něco nepřestalo fungovat, rapidně přibývá. Uvedené technice se říká Test driven development a je užitečné si vytvořit od začátku správné návyky.

Na unittesty použij normálně visual studio. Možná najdeš na netu tutorialy na testovací frameworky jako NUnit apod. Na ty se teď vyprdni. Jedeme na testovací nástroj co je ve VS.



Potřebné znalosti:
Práce s konozolí. 
Vytvoření Unit testu.

Use cases
=========

UC1:
Zadání bude jak na vietnamské kalkulačce
zadám číslo ENTER
zadám operátor +-/* ENTER
zadám další číslo ENTER
vyskočí výsledek

UC2:
můžu zadat číslo - tím se ignoruje výsledek z předchozího výpočtu    ENTER
můžu začít operátorem, který se aplikuje na poslední výsledek výpočtu     ENTER

UC3:
KOntrola vstupních hodnot
         - můžu zadat po sobě 2 čísla. Druhé číslo přebíjí předchozí
         - můžu zadat po sobě 2 operátory, poslední platí
         - namůžu zadat operátor a číslo, všechny vstupy oděluju ENTEREM
         - závorky nepodporujeme
         - pokud se vyskytne chyba, program vypíše chybovou hlášku.
         - pokud jsou v zadání nepovolené znaky, program vypíše chybovou hlášku.